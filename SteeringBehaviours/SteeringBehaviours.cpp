//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "SteeringBehaviours.h"

namespace SteerBehaviors 
{
	//seeks the target position
	void Seek(SteeringCharacter* character, const Math::Vector3D & targetPosition) 
	{
		//get the desired velocity
		Math::Vector3D desiredVelocity = targetPosition - character->GetWorldPosition();

		//seek at maximum velocity of the character
		desiredVelocity.Normalize();
		desiredVelocity *= character->GetMaximumSpeed();

		//get the steering force
		Math::Vector3D steer = desiredVelocity - character->GetVelocity();

		//and apply the force 
		character->ApplySteeringForce(steer);
	}

	//seeks the target 
	void Seek(SteeringCharacter* character, const SteeringTarget & target) 
	{
		Seek(character, target.position);
	}

	//seeks the target position but with unormalized velocity
	void UnormalizedSeek(SteeringCharacter* character, const Math::Vector3D & targetPosition)
	{
		//get the steering force
		Math::Vector3D steer = targetPosition - character->GetWorldPosition() - character->GetVelocity();

		//and apply the force (the character will clamp the force to its maximum allowed force)
		character->ApplySteeringForce(steer);
	}

	//seeks the target but with unormalized velocity
	void UnormalizedSeek(SteeringCharacter* character, const SteeringTarget & target)
	{
		UnormalizedSeek(character, target.position);
	}

	//flees away from the target position
	void Flee(SteeringCharacter* character, const Math::Vector3D & targetPosition)
	{
		//get the desired velocity
		Math::Vector3D desiredVelocity = targetPosition - character->GetWorldPosition();

		//seek at maximum velocity of the character
		desiredVelocity.Normalize();
		desiredVelocity *= character->GetMaximumSpeed();

		//get the steering force
		Math::Vector3D steer = character->GetVelocity() - desiredVelocity;

		//and apply the force 
		character->ApplySteeringForce(steer);
	}

	//flees away from the target
	void Flee(SteeringCharacter* character, const SteeringTarget & target)
	{
		Flee(character, target.position);
	}

	//flees away from the target position but with unormalized velocity
	void UnormalizedFlee(SteeringCharacter* character, const Math::Vector3D & targetPosition) 
	{
		//get the steering force
		Math::Vector3D steer = character->GetVelocity() - targetPosition + character->GetWorldPosition();

		//and apply the force (the character will clamp the force to its maximum allowed force)
		character->ApplySteeringForce(steer);
	}

	//flees away from the target but with unormalized velocity
	void UnormalizedFlee(SteeringCharacter* character, const SteeringTarget & target) 
	{
		UnormalizedFlee(character, target.position);
	}

	//a modification of Seek, will look at the velocity of the target, predict where he is going to be, and seeks this predicted position
	//the timeAheadForPrediction is the time that scales the velocity for predicting the future position of the target
	//timeAheadForPrediction should be keeped low to about 1 or 2 sec, so the predictions are made correctly.
	void Pursuit(SteeringCharacter* character, const SteeringTarget & target, Math::Float32 timeAheadForPrediction)
	{
		//get normalized velocity of the target
		Math::Vector3D targetVelociticy = target.GetVelocity();
		targetVelociticy.Normalize();

		//predict future move 
		Math::Vector3D futureTargetPos = target.position + targetVelociticy*timeAheadForPrediction;

		//now Seek future Pos
		Seek(character, futureTargetPos);
	}

	//a modification of Flee, will look at the velocity of the target, predict where he is going to be, and flees from this predicted position
	//the timeAheadForPrediction is the time that scales the velocity for predicting the future position of the target
	//timeAheadForPrediction should be keeped low to about 1 or 2 sec, so the predictions are made correctly.
	void Evade(SteeringCharacter* character, const SteeringTarget & target, Math::Float32 timeAheadForPrediction) 
	{
		//get normalized velocity of the target
		Math::Vector3D targetVelocity = target.GetVelocity();
		targetVelocity.Normalize();

		//predict future move 
		Math::Vector3D futureTargetPos = target.position + targetVelocity*timeAheadForPrediction;

		//now Flee from the future Pos:
		Flee(character, futureTargetPos);
	}

	//wandering is implemented by seeking a random position in the same plane as the character is. The random position is found by:
	// 1. predicting our future position with the current velocity and the timeAheadForPrediction
	// 2. randomly selecting a position in the circle that has center on the predicted position and radius wanderRadius
	void WanderSamePlane(SteeringCharacter* character, Math::Float32 wanderRadius, Math::Float32 timeAheadForPrediction) 
	{
		//predict future position
		Math::Vector3D futurePos = character->GetWorldPosition() + character->GetVelocity()*timeAheadForPrediction;

		//pick a random point in the circle XZ (assuming the character moves on that plane)  with center futurePos and radius wanderRadius
		Math::Vector2D randomInUnitCircle = Math::GenerateRandomInUnitCircle();
		Math::Vector3D randomSeekPos = futurePos + Math::Vector3D(randomInUnitCircle.x, futurePos.y, randomInUnitCircle.y)*wanderRadius;

		//seek the random pos 
		Seek(character, randomSeekPos);
	}

	//wandering is implemented by seeking a random position that is found by:
	// 1. predicting our future position with the current velocity and the timeAheadForPrediction
	// 2. randomly selecting a position in the circle that has center on the predicted position and radius wanderRadius
	void Wander3D(SteeringCharacter* character, Math::Float32 wanderRadius, Math::Float32 timeAheadForPrediction)
	{
		//predict future position
		Math::Vector3D futurePos = character->GetWorldPosition() + character->GetVelocity()*timeAheadForPrediction;

		//pick a random point in a sphere with center futurePos and radius wanderRadius
		Math::Vector3D randomSeekPos = futurePos + Math::GenerateRandomInUnitSphere()*wanderRadius;

		//seek the random pos 
		Seek(character, randomSeekPos);
	}

	//follows the path specified. 
	//This path is in 3D so at each path point it has defined a normal.
	//the normal of a line segment is interpolated by using the two point's normals that define the segment.
	//the timeAheadForPrediction is used for predicting the future position of the character from its current velocity
	void FollowPath(SteeringCharacter* character, const SteerPath & path, Math::Float32 timeAheadForPrediction) 
	{
		//predict the future location of our character with the current velocity
		Math::Vector3D futurePos = character->GetWorldPosition() + character->GetVelocity()*timeAheadForPrediction;

		//look for the line segment in the path that is closest to the future pos of the character
		//at the moment this is not optimized, and the search is done through all of the points (should work fine for short paths)
		//we use a min info structure to store the information about the minimum
		struct MinInfo 
		{
			Math::Float32 minD = FLT_MAX;			//stores the minimum distance
			Math::UnsgnInt minIdx = -1;				//stores the index of the segment point that gave us the minimum distance
			Math::Float32 minSegmentProp = 0;	    //stores the proportion of the segment that gave us the minimum distance, where the future pos 
			Math::Vector3D minSegmentDir = { 0,0,0 };	//stores the direction of the segment that gave us the minimum distance
		} minInfo;

		//iterate through all path segments
		for (Math::UnsgnInt i = 0; i < path.Length() - 1; ++i) 
		{
			//get the start and end point of a line segment in the path
			Math::Vector3D a = path.points[i];
			Math::Vector3D b = path.points[i + 1];

			//get the distance to the line segment in 3D
			//this should be checking for (b-a).Norm() != 0 but I will assume for now the paths have been carefully well designed...
			Math::Float32 d = ((futurePos - a).Cross(futurePos - b)).Norm() / (b - a).Norm();

			//keep track of the segment with minimum distance to the future pos of the character
			if (d < minInfo.minD) 
			{
				//but we also have to make sure the minimum distance point in the path, is actually on the path
				//so we get the proportion on the segment that our predicted pos is in
				Math::Vector3D segmentDir = (b - a);
				segmentDir.Normalize();
				Math::Float32 prop = (futurePos - a).Dot(segmentDir);

				//if the proportion is not in [0,1], the minimum distance point is not inside the segment [a,b] 
				if (prop<0 || prop>1)
					continue; //so we do not count those points and continue our search

				//otherwise we save our minimum distance information
				minInfo.minD = d;
				minInfo.minIdx = i;
				minInfo.minSegmentProp = prop;
				minInfo.minSegmentDir = segmentDir;
			}
		}

		//if the future pos is actually outside of all segments in the path
		if (minInfo.minIdx == -1) 
		{
			//seek the end of the path
			Seek(character, path.points[path.Length() - 1]);
			return;
		}

		//otherwise, check if the minimum distance is inside the radius of our steering path
		if (minInfo.minD < path.radius) 
		{
			//if we are, then we continue moving with the same velocity
			character->ApplySteeringVelocity(character->GetVelocity());
			return;
		}

		//otherwise we will seek a point in the path 
		//this is the path point P such that the segment [P, futurePos] is perpendicular to the path 
		//plus another random prediction but in the same direction as the segment direction
		Seek(character, path.points[minInfo.minIdx] + minInfo.minSegmentDir*(minInfo.minSegmentProp + Math::GenerateRandom(0.0f, minInfo.minSegmentProp - 1.0f)));
	}
}
