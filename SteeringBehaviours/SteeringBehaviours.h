//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef STEERING_BEHAVIOURS_H
#define STEERING_BEHAVIOURS_H

#include "SteeringCharacter.h"

//SteerBehaviours:
//this namespace encapsulate all steering behaviours that SteeringCharacter can use
namespace SteerBehaviors 
{
	//seeks the target position
	void Seek(SteeringCharacter* character, const Math::Vector3D & targetPosition);

	//seeks the target
	void Seek(SteeringCharacter* character, const SteeringTarget & target);

	//seeks the target position but with unormalized velocity
	void UnormalizedSeek(SteeringCharacter* character, const Math::Vector3D & targetPosition);

	//seeks the target but with unormalized velocity
	void UnormalizedSeek(SteeringCharacter* character, const SteeringTarget & target);

	//flees away from the target position
	void Flee(SteeringCharacter* character, const Math::Vector3D & targetPosition);

	//flees away from the target
	void Flee(SteeringCharacter* character, const SteeringTarget & target);

	//flees away from the target position but with unormalized velocity
	void UnormalizedFlee(SteeringCharacter* character, const Math::Vector3D & targetPosition);

	//flees away from the target but with unormalized velocity
	void UnormalizedFlee(SteeringCharacter* character, const SteeringTarget & target);

	//a modification of Seek, will look at the velocity of the target, predict where he is going to be, and seeks this predicted position
	//the timeAheadForPrediction is the time that scales the velocity for predicting the future position of the target
	//timeAheadForPrediction should be keeped low to about 1 or 2 sec, so the predictions are made correctly.
	void Pursuit(SteeringCharacter* character, const SteeringTarget & target, Math::Float32 timeAheadForPrediction);

	//a modification of Flee, will look at the velocity of the target, predict where he is going to be, and flees from this predicted position
	//the timeAheadForPrediction is the time that scales the velocity for predicting the future position of the target
	//timeAheadForPrediction should be keeped low to about 1 or 2 sec, so the predictions are made correctly.
	void Evade(SteeringCharacter* character, const SteeringTarget & target, Math::Float32 timeAheadForPrediction);

	//wandering is implemented by seeking a random position in the same plane as the character is. The random position is found by:
	// 1. predicting our future position with the current velocity and the timeAheadForPrediction
	// 2. randomly selecting a position in the circle that has center on the predicted position and radius wanderRadius
	void WanderSamePlane(SteeringCharacter* character, Math::Float32 wanderRadius, Math::Float32 timeAheadForPrediction);

	//wandering is implemented by seeking a random position that is found by:
	// 1. predicting our future position with the current velocity and the timeAheadForPrediction
	// 2. randomly selecting a position in the circle that has center on the predicted position and radius wanderRadius
	void Wander3D(SteeringCharacter* character, Math::Float32 wanderRadius, Math::Float32 timeAheadForPrediction);

	//follows the path specified. 
	//This path is in 3D so at each path point it has defined a normal.
	//the normal of a line segment is interpolated by using the two point's normals that define the segment.
	//the timeAheadForPrediction is used for predicting the future position of the character from its current velocity
	void FollowPath(SteeringCharacter* character, const SteerPath & path, Math::Float32 timeAheadForPrediction);
}

#endif
