//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "BTInverterNode.h"

//called when entering this behaviour node (before the run method starts on ticking).
void BTInverterNode::OnBehaviorStart(AICharacter* agent) 
{
	m_Node->OnBehaviorStart(agent);
}

//update method for this behaviour node. 
//returns the opposite of the wrapped node, or running if still running
BehaviourStatus BTInverterNode::OnBehaviourTick(AICharacter* agent) 
{
	//executes only the wrapped node
	BehaviourStatus status = m_Node->OnBehaviourTick(agent);

	//if running, then returns the same
	if (status == BehaviourStatus::BT_RUNNING) 
		return BehaviourStatus::BT_RUNNING;

	//otherwise invert the node
	return (status == BehaviourStatus::BT_SUCCESS ? BehaviourStatus::BT_FAILURE : BehaviourStatus::BT_SUCCESS);
}
