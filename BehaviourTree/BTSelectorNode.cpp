//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "BTSelectorNode.h"

//called when entering this behaviour node (before the run method starts on ticking).
void BTSelectorNode::OnBehaviorStart(AICharacter* agent) 
{
	//start on the first node of the list
	m_CurrentNodeIdx = 0;

	//start the first behaviour
	m_Nodes[0]->OnBehaviorStart(agent);
}

//update method for this behaviour node. 
//this delegates the tick to a current node in the sequence. 
//Returns true when the current node succeed. running if the current node fails. failure if all nodes in the sequence fail.
BehaviourStatus BTSelectorNode::OnBehaviourTick(AICharacter* agent) 
{
	//check the sequence is not empty 
	if (m_Nodes.empty()) 
		return BehaviourStatus::BT_SUCCESS;

	//run the current node
	BehaviourStatus currentNodeStatus = m_Nodes[m_CurrentNodeIdx]->OnBehaviourTick(agent);

	//filter the status of the current node: if running or success, then notify that status to the caller
	if (currentNodeStatus == BehaviourStatus::BT_RUNNING || currentNodeStatus == BehaviourStatus::BT_SUCCESS) 
		return currentNodeStatus;

	//otherwise the current node FAILED! go to next node in the sequence, if there aren't any more nodes, return failure!
	else 
	{
		//go to next node (modifies the index)
		GoToNextNode();

		//return failure if there are no more nodes
		if (m_CurrentNodeIdx >= m_Nodes.size()) 
			return BehaviourStatus::BT_FAILURE;

		//otherwise enter new node
		m_Nodes[m_CurrentNodeIdx]->OnBehaviorStart(agent);

		//and return we are still running
		return BehaviourStatus::BT_RUNNING;
	}
}

//gets the next node index from the current node index
//in a simple selector, this just increment the index
void BTSelectorNode::GoToNextNode() 
{
	m_CurrentNodeIdx++;
}

