//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef BH_TREE_CONSTANT_STATUS_NODE_H
#define BH_TREE_CONSTANT_STATUS_NODE_H

#include "BehaviourTreeNode.h"

//BTConstantStatusNode:
//A Behaviour Tree node that always returns a constant status.
//this implements all three: 
//    *Succeder Node (returns always BT_SUCCESS), 
//    *Failure  Node (returns always BT_FAILURE),
//    *Runner   Node (returns always BT_RUNNING)
//the constant status can be defined on the CTOR.
//The node start and end methods do nothing but they can be overriden. 
template <BehaviourStatus BT_STATUS>
class BTConstantStatusNode : public BTDecoratorNode 
{
public:
	//called when entering this behaviour node (before the run method starts on ticking).
	virtual void OnBehaviorStart(AICharacter* agent) override 
	{
		m_Node->OnBehaviorStart(agent);
	}

	//update method for this behaviour node. 
	//returns always a constant behaviour after executing the wrapped node
	BehaviourStatus OnBehaviourTick(AICharacter* agent) override 
	{
		//executes the wrapped node
		m_Node->OnBehaviourTick(agent);

		//and returns the constant state
		return BT_STATUS;
	}
};

//returns always BT_SUCCESS
typedef BTConstantStatusNode<BehaviourStatus::BT_SUCCESS> BTSuccederNode;

//returns always BT_FAILURE
typedef BTConstantStatusNode<BehaviourStatus::BT_FAILURE> BTFailureNode;

//returns always BT_RUNNING
typedef BTConstantStatusNode<BehaviourStatus::BT_RUNNING> BTRunnerNode;

#endif
