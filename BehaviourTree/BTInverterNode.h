//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef BH_TREE_INVERTER_NODE_H
#define BH_TREE_INVERTER_NODE_H

#include "BehaviourTreeNode.h"

//BTInverterNode: Behaviour Tree Inverter Node
//Inverts the status of a current node, or return running if the node is still running
class BTInverterNode : public BTDecoratorNode 
{
public:
	//called when entering this behaviour node (before the run method starts on ticking).
	void OnBehaviorStart(AICharacter* agent) override;

	//update method for this behaviour node. 
	//returns the opposite of the wrapped node, or running if still running
	BehaviourStatus OnBehaviourTick(AICharacter* agent) override;
};

#endif
