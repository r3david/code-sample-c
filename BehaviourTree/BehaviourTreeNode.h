//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef BH_TREE_NODE_H
#define BH_TREE_NODE_H

#include <vector>
#include "AICharacter.h"

//this is all possible status of a behaviour node tick
enum class BehaviourStatus 
{
	BT_SUCCESS,
	BT_FAILURE,
	BT_RUNNING
};

//BTNode: Behaviour Tree Node
//This is the interface that all Behavior Tree nodes must implement
class BTNode 
{
public:
	//virtual DTOR (best practice)
	virtual ~BTNode() {};

	//called when entering this behaviour node (before the run method starts on ticking).
	virtual void OnBehaviorStart(AICharacter* agent) = 0;

	//update method for this behaviour node. 
	//it returns the status of this behaviour node, used by the caller for transiting to another behaviours
	virtual BehaviourStatus OnBehaviourTick(AICharacter* agent) = 0;
};

//BTDecoratorNode: Behaviour Tree Decorator Node
//base class for a decorator node for the behaviour trees
class BTDecoratorNode : public BTNode 
{
protected:
	//wrapped node for this decorator
	BTNode* m_Node;

public:
	//CTOR:
	BTDecoratorNode(BTNode* node) 
	{
		m_Node = node;
	};

	//Disable copy constructor
	BTDecoratorNode(const BTDecoratorNode & other) = delete;

	//disable assignment operator
	BTDecoratorNode& operator=(const BTDecoratorNode & other) = delete;

	//DTOR: deallocate memory for the wrapped node
	virtual ~BTDecoratorNode() 
	{
		delete m_Node;
	}

	//called when entering this behaviour node (before the run method starts on ticking).
	virtual void OnBehaviorStart(AICharacter* agent) = 0;

	//update method for this behaviour node. 
	//it returns the status of this behaviour node, used by the caller for transiting to another behaviours
	virtual BehaviourStatus OnBehaviourTick(AICharacter* agent) = 0;
};

//BTCompositeNode: Behaviour Tree Composite Node
//This is the interface that all composite Behavior Tree nodes must implement
//A composite node is composed by a list of other nodes
class BTCompositeNode : public BTNode 
{
protected:
	//array of nodes that compose this node
	std::vector<BTNode*> m_Nodes;

public:
	//disable copy constructor
	BTCompositeNode(const BTCompositeNode & other) = delete;

	//disable assignment operator
	BTCompositeNode& operator=(const BTCompositeNode & other) = delete;

	//DTOR: deallocate memory for all nodes in this composite node
	virtual ~BTCompositeNode() 
	{
		std::vector<BTNode*>::iterator iter;
		for (iter = m_Nodes.begin(); iter != m_Nodes.end(); ++iter) 
		{
			delete *iter;
		}
	}

	//called when entering this behaviour node (before the run method starts on ticking).
	virtual void OnBehaviorStart(AICharacter* agent) = 0;

	//update method for this behaviour node. 
	//it returns the status of this behaviour node, used by the caller for transiting to another behaviours
	virtual BehaviourStatus OnBehaviourTick(AICharacter* agent) = 0;

	//Adds a child to this node. Takes ownership of the child.
	virtual BTCompositeNode* AddNode(BTNode* node) 
	{
		m_Nodes.push_back(node);
		return this;
	};
};

#endif

