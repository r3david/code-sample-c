//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "BTRepeaterNode.h"

//called when entering this behaviour node (before the run method starts on ticking).
//start all nodes
void BTRepeaterNode::OnBehaviorStart(AICharacter* agent) 
{
	for (auto & n : m_Nodes) 
		n->OnBehaviorStart(agent);
}

//update method for this behaviour node. 
//this ticks all children nodes and repeats when finishing its behaviours
//Returns always running, and repeats the nodes behaviour, when the node return success or failure
BehaviourStatus BTRepeaterNode::OnBehaviourTick(AICharacter* agent) 
{
	//iterate all nodes
	for (auto & n : m_Nodes) 
	{
		//tick the node and get its status
		auto status = n->OnBehaviourTick(agent);

		//if the node succeded or failed
		if (status != BehaviourStatus::BT_RUNNING) 
		{
			//start again its behaviour
			n->OnBehaviorStart(agent);
		}
	}

	//return always running
	return BehaviourStatus::BT_RUNNING;
}
