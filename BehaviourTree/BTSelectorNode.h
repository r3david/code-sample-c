//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef BH_TREE_SELECTOR_NODE_H
#define BH_TREE_SELECTOR_NODE_H

#include "Maths.h"
#include "BehaviourTreeNode.h"

//BTSelectorNode: 
//executes a sequence of nodes (not all in the same tick)
//Returns true whenever the current node in the sequence succeed. failure when all nodes fail.
class BTSelectorNode : public BTCompositeNode 
{
protected:
	//the index of the current node of the sequence that is running at the moment
	Math::UnsgnInt m_CurrentNodeIdx;
public:
	//virtual DTOR : best practice
	virtual ~BTSelectorNode() {}

	//called when entering this behaviour node (before the run method starts on ticking).
	void OnBehaviorStart(AICharacter* agent) override;

	//update method for this behaviour node. 
	//this delegates the tick to a current node in the sequence. 
	//Returns true when a current node returns true, failure if all nodes in the sequence fails
	BehaviourStatus OnBehaviourTick(AICharacter* agent) override;

protected:
	//gets the next node index from the current node index (can be overriden to navigate nodes in different ways)
	virtual void GoToNextNode();

};

#endif
