//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef RIC_INFERENCE_SA_H
#define RIC_INFERENCE_SA_H

#include "../../GlobalSettings.h"
#include "../../VineModel/VineModelData.h"
#include "../../VineModel/VineModelDef.h"
#include "../StructureInferenceInterface.h"

namespace InferenceSpace 
{

#pragma region SA MOVE CLASS AND ACCEPT PROBABILITY

	//this is the move class for simulated annealing. It modifies a current state to get a new state
	struct SAStructureModifier 
	{
	private:
		//reference to the vine model data used for moving between states
		const VineModel::VineMarkovModel & VINE_MODEL_REF;
	public:
		//CTOR
		SAStructureModifier(const VineModel::VineMarkovModel & VM) : VINE_MODEL_REF(VM) {};

		//Modifies the state argument, getting a new state
		//true if success, false otherwise
		bool GetNextState(StructureState & new_state) const;

	private:
		//The new state is a single join set at random
		bool NextStateUsingSingleRandomJoinSwap(StructureState & new_state) const;

		//The new state is a single join and flow set at random
		bool NextStateUsingSingleRandomJoinAndFlowSwap(StructureState & new_state) const;

		//the new state is a random group swap: iterate through all joins of a random group and change randomly their state
		bool NextStateUsingSingleRandomJoinAndFullCaneFlowSwap(StructureState & new_state) const;
	};

	//this is the acceptance probability functor for simulated annealing
	struct SAAcceptanceProbability 
	{
	public:
		double Eval(double current_energy, double new_energy, double temp) const;
	};

#pragma endregion

	//This is the implementation of the structure inference interface that uses Simulated annealing
	class InferenceSA : public StructureInferenceInterface 
	{
	public:
		InferenceSA(const VineModel::VineMarkovModel & vm) : StructureInferenceInterface(vm) {};
		void GetStructure(StructureState & structure_state, std::ofstream & energy_report_file) const override;
	};

}

#endif
