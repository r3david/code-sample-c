//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "Inference_SA.h"
#include "SimulatedAnnealing.h"

namespace InferenceSpace 
{

#pragma region SA MOVE CLASS AND ACCEPT PROBABILITY

	bool SAStructureModifier::GetNextState(StructureState & new_state)const 
	{
		switch (GlobalSettings::SA_MOVE_CLASS) 
		{
			case GlobalSettings::SINGLE_RANDOM_JOIN: return NextStateUsingSingleRandomJoinSwap(new_state);
			case GlobalSettings::SINGLE_RANDOM_JOIN_AND_FLOW: return NextStateUsingSingleRandomJoinAndFlowSwap(new_state);
			case GlobalSettings::SINGLE_JOIN_FULL_CANE_FLOW: return NextStateUsingSingleRandomJoinAndFullCaneFlowSwap(new_state);
			default: return NextStateUsingSingleRandomJoinSwap(new_state);
		}
	}

	bool SAStructureModifier::NextStateUsingSingleRandomJoinSwap(StructureState & new_state) const 
	{
		//get a random join and change its state
		static cv::RNG rng(12345);
		size_t random_node_index = (size_t)floor(rng.uniform(0.0, (double)VINE_MODEL_REF.nodes.size()));
		
		while (VINE_MODEL_REF.nodes[random_node_index].data.type == VineModel::ConnectionCandidateType::CONNECTED)
		{
			random_node_index++;
			if (random_node_index >= VINE_MODEL_REF.nodes.size()) 
				random_node_index = 0;
		}

		//get a random state for this join
		int state_val = rng.uniform(0.0, 1.0) < 0.5 ? 0 : 1;
		new_state.j_state[VINE_MODEL_REF.nodes[random_node_index].join_idx] = state_val;

		return true;
	}

	bool SAStructureModifier::NextStateUsingSingleRandomJoinAndFlowSwap(StructureState & new_state) const 
	{
		//get a random join and change its state
		static cv::RNG rng(12345);
		size_t random_node_index = (size_t)floor(rng.uniform(0.0, (double)VINE_MODEL_REF.nodes.size()));
		
		while (VINE_MODEL_REF.nodes[random_node_index].data.type == VineModel::ConnectionCandidateType::CONNECTED) 
		{
			random_node_index++;
			if (random_node_index >= VINE_MODEL_REF.nodes.size()) 
				random_node_index = 0;
		}

		int state_val = rng.uniform(0.0, 1.0) < 0.5 ? 0 : 1;
		int flow_val = rng.uniform(0.0, 1.0) < 0.5 ? 0 : 1;

		new_state.j_state[VINE_MODEL_REF.nodes[random_node_index].join_idx] = state_val;
		new_state.f_state[VINE_MODEL_REF.nodes[random_node_index].join_idx] = flow_val;

		return true;
	}

	bool SAStructureModifier::NextStateUsingSingleRandomJoinAndFullCaneFlowSwap(StructureState & new_state) const 
	{
		//first get a random modification type
		static cv::RNG rng(12345);
		int mod_type = rng.uniform(0.0, 1.0) < 0.5 ? 0 : 1;

		//single random join swap
		if (mod_type == 0) 
		{
			size_t random_node_index = (size_t)floor(rng.uniform(0.0, (double)VINE_MODEL_REF.nodes.size()));
			size_t init_node_index = random_node_index;
			
			while (VINE_MODEL_REF.nodes[random_node_index].data.type == VineModel::ConnectionCandidateType::CONNECTED) 
			{
				random_node_index++;
				if (random_node_index >= (size_t)VINE_MODEL_REF.nodes.size())
					random_node_index = 0;
				
				if (random_node_index == init_node_index) 
				{
					//means all nodes are of type connected, return false
					return false;
				}
			}

			int state_val = rng.uniform(0.0, 1.0) < 0.5 ? 0 : 1;
			new_state.j_state[VINE_MODEL_REF.nodes[random_node_index].join_idx] = state_val;
		}
		//full cane flow swap
		else 
		{
			size_t random_node_index = (size_t)floor(rng.uniform(0.0, (double)VINE_MODEL_REF.nodes.size()));
			size_t target_flow_value = rng.uniform(0.0, 1.0) < 0.5 ? 0 : 1;

			//make sure the random node is in a connected state, otherwise, if all nodes are unconnected, we set the random node to connected and set its flow 
			size_t init_random_node = random_node_index;
			while (new_state.j_state[random_node_index] != 1) 
			{
				random_node_index++;
				if (random_node_index >= (size_t)VINE_MODEL_REF.nodes.size()) 
					random_node_index = 0;
				if (random_node_index == init_random_node) 
				{
					new_state.j_state[random_node_index] = 1;
					new_state.f_state[random_node_index] = target_flow_value;
					return true;
				}
			}

			//propagate the cane flow
			new_state.PropagateFullCaneFlow(random_node_index, target_flow_value, VINE_MODEL_REF);
		}
		return true;
	}

	double SAAcceptanceProbability::Eval(double current_energy, double new_energy, double temp) const 
	{
		return new_energy < current_energy ? 1.0 : exp(-(new_energy - current_energy) / temp);
	}

#pragma endregion

	void InferenceSA::GetStructure(StructureState & structure_state, std::ofstream & energy_report_file) const 
	{
		//structures needed for simulated annealing
		SAStructureModifier jump_machine(_vine_model);
		SAAcceptanceProbability jump_prob;
		VineModel::EnergyModel energy_function(_vine_model);

		//we keep track of the energy at all iterations
		double best_energy = DBL_MAX;
		StructureState best_state;

		//run SIMULATED ANNEALING WITH RANDOM INITIALIZATIONS
		for (size_t i = 0; i < GlobalSettings::SAN_RANDOM_INITIALIZATIONS; ++i) 
		{
			//initialization
			StructureState start_state;
			StateRandomInitialization(start_state, _vine_model.nodes.size());

			//run simulated annealing
			using OptimizationMethods::SimulatedAnnealing;
			using OptimizationMethods::DEFAULT_SA_OPTS;
			SimulatedAnnealing<StructureState, SAStructureModifier, SAAcceptanceProbability, VineModel::EnergyModel> sa(DEFAULT_SA_OPTS, start_state, jump_machine, jump_prob, energy_function);
			double energy = sa.Run(energy_report_file);

			//see if we got a better state and energy
			if (energy<best_energy) 
			{
				best_energy = energy;
				best_state = sa.state;
			}
		}

		//set the output state to the best state found in icm
		structure_state = best_state;
	}

}