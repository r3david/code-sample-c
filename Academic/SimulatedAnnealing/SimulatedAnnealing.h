//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef RIC_SIM_ANNEALING_H
#define RIC_SIM_ANNEALING_H

#include <random>
#include <iostream>
#include <fstream>

namespace OptimizationMethods 
{

#pragma region OPTIONS
	//Options for Simulated Annealing
	struct SAOptions 
	{
		double t_start;
		double t_min;
		double alpha;
		int max_iter;
	};

	//default options for simulated annealing
	static const SAOptions DEFAULT_SA_OPTS = { 2.0, 0.001, 0.8 , 150 }; //2.0, 0.005, 0.8 , 100

#pragma endregion

	template<class StateType, class StateModifier, class AcceptanceProbability, class EnergyFunction>
	class SimulatedAnnealing 
	{
	public:
		SAOptions opts;
		StateType state; //this is a copy of the start state we pass on the CTOR. Afterwards, it gets changed during Run Method, and will contain the best state at the end of Run Method
		StateModifier state_modifier;
		AcceptanceProbability accep_prob;
		EnergyFunction energy_function;

	public:
		SimulatedAnnealing(SAOptions opt, StateType start_state, StateModifier move_function, AcceptanceProbability ap, EnergyFunction e) : opts(opt), state(start_state), state_modifier(move_function), accep_prob(ap), energy_function(e) 
		{
		};

		~SimulatedAnnealing() 
		{
		};

		double Run(std::ofstream & energy_report_file);
	};

	//Instantiation of SimulatedAnnealing::Run Method
	template<class StateType, class StateModifier, class AcceptanceProbability, class EnergyFunction>
	double SimulatedAnnealing<StateType, StateModifier, AcceptanceProbability, EnergyFunction>::Run(std::ofstream & energy_report_file) 
	{
		//NEEDED TEMPORARY VARIABLES
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_real_distribution<> u_distribution(0, 1);

		//INITIAL VALUES
		double energy = energy_function.GetEnergy(state);
		double temp = opts.t_start;

		//WE KEEP TRACK OF THE BEST STATE REACHED AT MINIMUM ENERGY
		StateType best_state, next_state;
		best_state = state;
		double min_energy = energy;

		while (temp > opts.t_min) 
		{
			int iter = 1;
			while (iter <= opts.max_iter) 
			{
				next_state = state;

				if (!state_modifier.GetNextState(next_state)) 
				{
					iter += 1;
					continue;
				}

				double new_energy = energy_function.GetEnergy(next_state);

				//decide if we want to go to the new modified state or stay with the same structure unchanged
				double ap = accep_prob.Eval(energy, new_energy, temp);
				double u = u_distribution(gen);
				if (ap > u) 
				{
					state = next_state;
					energy = new_energy;

					if (energy < min_energy) 
					{
						min_energy = energy;
						best_state = state;
					}
				}
				iter += 1;

				if (GlobalSettings::USE_ENERGY_REPORT_FILE) 
				{
					if (energy_report_file.is_open()) 
					{
						energy_report_file << "ITERATION \t " << iter;
						energy_report_file << "\t TEMP \t " << temp;
						energy_report_file << "\t ENERGY \t " << energy << std::endl;
					}
				}
			}
			temp = temp*opts.alpha;
		}

		state = best_state;
		return min_energy;
	}

}
#endif