//-----------------------------------------------------------------------------
// Copyright (c) 2013 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

// NEEDS OpenCV 2.49 
#include <opencv2\opencv.hpp>
#include <vector>
#include "slic.h"

int main(int argc, char *argv[]) 
{
	// Load the image and convert to Lab colour space.
	cv::Mat image = cv::imread("bird.jpg", 1);
	cv::Mat lab_image;
	cv::cvtColor(image, lab_image, CV_BGR2Lab);

	// Yield the number of superpixels and weight-factors from the user. 
	int w = image.cols, h = image.rows;
	int nr_superpixels = 500;
	int nc = 40;
	int step = cvRound(sqrt((w * h) / (double)nr_superpixels));

	// Perform the SLIC superpixel algorithm. 
	SlicSpace::Slic slic;
	slic.generateSuperpixels(lab_image, step, nc);
	slic.createConnectivity(lab_image);

	// Display the contours and show the result. 
	slic.renderClusterWithMeanColor(image);
	slic.renderContours(image, cv::Vec3b(0, 0, 0));
	cv::imshow("RESULT", image);
	cvWaitKey(0);
	cv::imwrite("result.jpg", image);
}