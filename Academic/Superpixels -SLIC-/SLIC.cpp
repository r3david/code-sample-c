//-----------------------------------------------------------------------------
// Copyright (c) 2013 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "SLIC.h"

namespace SlicSpace 
{

	void Slic::initialize(const cv::Mat & image) 
	{
		_clusters_map = cv::Mat::ones(image.rows, image.cols, cv::DataType<int>::type) * (-1);
		_distances = cv::Mat::ones(image.rows, image.cols, cv::DataType<double>::type) * (FLT_MAX);


		/* Initialize the centers and counters. */
		for (int i = _step; i < image.cols - _step / 2; i += _step) 
		{
			for (int j = _step; j < image.rows - _step / 2; j += _step) 
			{
				std::vector<double> center_data;

				/* Find the local minimum (gradient-wise). */
				cv::Point ncenter = findLocalMinGrad(image, cv::Point(i, j));
				cv::Vec3b colour = image.at<cv::Vec3b>(ncenter.y, ncenter.x);

				/* Generate the center vector. */
				center_data.push_back(colour.val[0]);
				center_data.push_back(colour.val[1]);
				center_data.push_back(colour.val[2]);
				center_data.push_back(ncenter.x);
				center_data.push_back(ncenter.y);

				/* Append to vector of centers. */
				_centers.push_back(center_data);
				_center_counts.push_back(0);
			}
		}
	}

	cv::Point Slic::findLocalMinGrad(const cv::Mat & image, const cv::Point & center) const 
	{
		double min_grad = FLT_MAX;
		cv::Point loc_min = cv::Point(center.x, center.y);

		for (int i = center.x - 1; i < center.x + 2; ++i) 
		{
			for (int j = center.y - 1; j < center.y + 2; ++j) 
			{
				cv::Vec3b c1 = image.at<cv::Vec3b>(j + 1, i);
				cv::Vec3b c2 = image.at<cv::Vec3b>(j, i + 1);
				cv::Vec3b c3 = image.at<cv::Vec3b>(j, i);

				/* Convert colour values to grayscale values. */
				double i1 = static_cast<double>(c1.val[0]);//just blue channel for now
				double i2 = static_cast<double>(c2.val[0]);
				double i3 = static_cast<double>(c3.val[0]);

				/*double i1 = c1.val[0] * 0.11 + c1.val[1] * 0.59 + c1.val[2] * 0.3;
				double i2 = c2.val[0] * 0.11 + c2.val[1] * 0.59 + c2.val[2] * 0.3;
				double i3 = c3.val[0] * 0.11 + c3.val[1] * 0.59 + c3.val[2] * 0.3;*/

				/* Compute horizontal and vertical gradients and keep track of the minimum. */
				if (sqrt(pow(i1 - i3, 2)) + sqrt(pow(i2 - i3, 2)) < min_grad) 
				{
					min_grad = fabs(i1 - i3) + fabs(i2 - i3);
					loc_min.x = i;
					loc_min.y = j;
				}
			}
		}
		return loc_min;
	}

	double Slic::computeDist(const int & ci, const cv::Point & pixel, const cv::Vec3b & colour) const 
	{
		double dc = sqrt(pow(_centers[ci][0] - colour.val[0], 2) + pow(_centers[ci][1] - colour.val[1], 2) + pow(_centers[ci][2] - colour.val[2], 2));
		double ds = sqrt(pow(_centers[ci][3] - pixel.x, 2) + pow(_centers[ci][4] - pixel.y, 2));
		return sqrt(pow(dc / _nc, 2) + pow(ds / _ns, 2));
	}

	void Slic::generateSuperpixels(const cv::Mat & image, const int & step, const int & nc) 
	{
		_step = step;
		_nc = nc;
		_ns = step;

		initialize(image);

		// Run EM for 10 iterations (as prescribed by the algorithm).
		for (int i = 0; i < NR_ITERATIONS; ++i) 
		{
			// Reset distance values.
			_distances = cv::Mat::ones(image.rows, image.cols, cv::DataType<double>::type) * (FLT_MAX);

			for (int j = 0; j < static_cast<int>(_centers.size()); ++j) 
			{
				// Only compare to pixels in a 2 x step by 2 x step region.
				for (int k = cvRound(_centers[j][3]) - step; k < cvRound(_centers[j][3]) + step; k++) 
				{
					for (int l = cvRound(_centers[j][4]) - step; l < cvRound(_centers[j][4]) + step; l++) 
					{
						if (k >= 0 && k < image.cols && l >= 0 && l < image.rows) 
						{
							cv::Vec3b colour = image.at<cv::Vec3b>(l, k);
							double d = computeDist(j, cv::Point(k, l), colour);

							// Update cluster allocation if the cluster minimizes the distance.
							if (d < _distances.at<double>(l, k)) 
							{
								_distances.at<double>(l, k) = d;
								_clusters_map.at<int>(l, k) = j;
							}
						}
					}
				}
			}

			// Clear the center values.
			for (size_t j = 0; j < _centers.size(); ++j) 
			{
				_centers[j][0] = _centers[j][1] = _centers[j][2] = _centers[j][3] = _centers[j][4] = 0;
				_center_counts[j] = 0;
			}

			// Compute the new cluster centers. 
			for (int j = 0; j < image.cols; ++j) 
			{
				for (int k = 0; k < image.rows; ++k) 
				{
					int c_id = _clusters_map.at<int>(k, j);

					if (c_id != -1) 
					{
						cv::Vec3b colour = image.at<cv::Vec3b>(k, j);

						_centers[c_id][0] += colour.val[0];
						_centers[c_id][1] += colour.val[1];
						_centers[c_id][2] += colour.val[2];
						_centers[c_id][3] += j;
						_centers[c_id][4] += k;

						_center_counts[c_id] += 1;
					}
				}
			}

			// Normalize the clusters.
			for (size_t j = 0; j < _centers.size(); ++j) 
			{
				_centers[j][0] /= _center_counts[j];
				_centers[j][1] /= _center_counts[j];
				_centers[j][2] /= _center_counts[j];
				_centers[j][3] /= _center_counts[j];
				_centers[j][4] /= _center_counts[j];
			}
		}
	}

	void Slic::createConnectivity(const cv::Mat & image) 
	{
		int label = 0, adjlabel = 0;
		const int lims = (image.rows * image.cols) / (static_cast<int>(_centers.size()));

		const int dx4[4] = { -1,  0,  1,  0 };
		const int dy4[4] = { 0, -1,  0,  1 };

		// Initialize the new cluster matrix.
		cv::Mat new_clusters = cv::Mat::ones(image.rows, image.cols, cv::DataType<int>::type)*(-1);

		for (int i = 0; i < image.cols; ++i) 
		{
			for (int j = 0; j < image.rows; ++j) 
			{
				if (new_clusters.at<int>(j, i) == -1) 
				{
					std::vector<cv::Point> elements;
					elements.push_back(cv::Point(i, j));

					// Find an adjacent label, for possible use later. 
					for (int k = 0; k < 4; ++k) 
					{
						int x = elements[0].x + dx4[k], y = elements[0].y + dy4[k];

						if (x >= 0 && x < image.cols && y >= 0 && y < image.rows) 
						{
							if (new_clusters.at<int>(y, x) >= 0) 
							{
								adjlabel = new_clusters.at<int>(y, x);
							}
						}
					}

					int count = 1;
					for (int c = 0; c < count; c++) 
					{
						for (int k = 0; k < 4; k++) 
						{
							int x = elements[c].x + dx4[k], y = elements[c].y + dy4[k];

							if (x >= 0 && x < image.cols && y >= 0 && y < image.rows) 
							{
								if (new_clusters.at<int>(y, x) == -1 && _clusters_map.at<int>(j, i) == _clusters_map.at<int>(y, x)) 
								{
									elements.push_back(cv::Point(x, y));
									new_clusters.at<int>(y, x) = label;
									count += 1;
								}
							}
						}
					}

					// Use the earlier found adjacent label if a segment size is smaller than a limit.
					if (count <= lims >> 2) 
					{
						for (int c = 0; c < count; c++) 
						{
							new_clusters.at<int>(elements[c].y, elements[c].x) = adjlabel;
						}
						label -= 1;
					}
					label += 1;
				}
			}
		}
	}

	void Slic::renderClusterCenters(cv::Mat & image, const cv::Scalar & colour) const 
	{
		for (auto & p : _centers) 
		{
			cv::circle(image, cv::Point(cvRound(p[3]), cvRound(p[4])), 1, colour, 2);
		}
	}

	void Slic::renderContours(cv::Mat & image, const cv::Vec3b & colour) const 
	{
		const int dx8[8] = { -1, -1,  0,  1, 1, 1, 0, -1 };
		const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1 };

		// Initialize the contour vector and the matrix detailing whether a pixel is already taken to be a contour.
		std::vector<cv::Point> contours;
		cv::Mat istaken = cv::Mat::zeros(image.rows, image.cols, cv::DataType<bool>::type);

		// Go through all the pixels.
		for (int i = 0; i < image.cols; ++i) 
		{
			for (int j = 0; j < image.rows; ++j) 
			{
				int nr_p = 0;

				// Compare the pixel to its 8 neighbours.
				for (int k = 0; k < 8; ++k) 
				{
					int x = i + dx8[k], y = j + dy8[k];

					if (x >= 0 && x < image.cols && y >= 0 && y < image.rows) 
					{
						if (!istaken.at<bool>(y, x) && _clusters_map.at<int>(j, i) != _clusters_map.at<int>(y, x)) 
						{
							nr_p += 1;
						}
					}
				}

				// Add the pixel to the contour list if desired.
				if (nr_p >= 2) 
				{
					contours.push_back(cv::Point(i, j));
					istaken.at<bool>(j, i) = true;
				}
			}
		}

		// Draw the contour pixels.
		for (auto & p : contours) 
		{
			image.at<cv::Vec3b>(p.y, p.x) = colour;
		}
	}

	void Slic::renderClusterWithMeanColor(cv::Mat & image) const 
	{
		int img_type = image.type();
		
		if (img_type != CV_32FC3) 
			image.convertTo(image, CV_32FC3, 1.0 / 255.0);

		std::vector<cv::Vec3f> colours(_centers.size());

		// Gather the colour values per cluster.
		for (int row = 0; row < image.rows; ++row) 
		{
			for (int col = 0; col < image.cols; ++col) 
			{
				cv::Vec3f colour = image.at<cv::Vec3f>(row, col);
				colours[_clusters_map.at<int>(row, col)] += colour;
			}
		}

		// Divide by the number of pixels per cluster to get the mean colour. 
		for (size_t i = 0; i < colours.size(); i++) 
		{
			colours[i] /= _center_counts[i];
		}

		// Fill in.
		for (int row = 0; row < image.rows; row++) 
		{
			for (int col = 0; col < image.cols; col++) 
			{
				image.at<cv::Vec3f>(row, col) = colours[_clusters_map.at<int>(row, col)];
			}
		}

		if (img_type != CV_32FC3) 
			image.convertTo(image, img_type, 255);
	}

}