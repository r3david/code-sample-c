//-----------------------------------------------------------------------------
// Copyright (c) 2013 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef SLIC_H
#define SLIC_H

#include <opencv2\opencv.hpp>
#include <vector>

namespace SlicSpace 
{

#define NR_ITERATIONS 10 // The number of iterations run by the clustering algorithm.

	/*
	* class Slic.
	* In this class, an over-segmentation is created of an image, provided by the
	* step-size (distance between initial cluster locations) and the colour
	* distance parameter.
	*/
	class Slic 
	{
	private:
		cv::Mat _clusters_map; 
		cv::Mat _distances; 
		std::vector< std::vector<double> > _centers; // The LAB and xy values of the centers.
		std::vector<int> _center_counts; // The number points assigned to each cluster
		int _step, _nc, _ns; // The step size per cluster, and the colour (nc) and distance (ns) parameters.

		double computeDist(const int & ci, const cv::Point & pixel, const cv::Vec3b & colour) const; // Compute the distance between a center and an individual pixel. 
		cv::Point findLocalMinGrad(const cv::Mat & image, const cv::Point & center) const; // Find the pixel with the lowest gradient in a 3x3 neighbors
		void initialize(const cv::Mat & image); // Remove and initialize the 2d vectors. 

	public:
		void generateSuperpixels(const cv::Mat & image, const int & step, const int & nc);
		void createConnectivity(const cv::Mat & image);
		void renderClusterCenters(cv::Mat & image, const cv::Scalar & colour) const; // Draw functions. Resp. displayal of the centers and the contours.
		void renderContours(cv::Mat & image, const cv::Vec3b & colour) const;
		void renderClusterWithMeanColor(cv::Mat & image) const;
	};
}

#endif