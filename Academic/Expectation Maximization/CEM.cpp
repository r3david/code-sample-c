//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "CEM.h"
#include <set>
#include <stdexcept> 

template <class ModelType>
CEM<ModelType>::CEM(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence) : m_Data(Data), m_EMInitializer(emInit), m_MoG(MoG) 
{
	try 
	{
		switch (convergence) 
		{
		case EM_MAXITER:
			m_convCriteria = boost::shared_ptr<EMConvCriteriaBase<ModelType> >(new EMConvCriteriaBase<ModelType>());
			break;
		case EM_LOGL:
			m_convCriteria = boost::shared_ptr<EMConvCriteriaBase<ModelType> >(new CEMLOGL_Conv<ModelType>(Data));
			break;
		case EM_PARAMS:
			m_convCriteria = boost::shared_ptr<EMConvCriteriaBase<ModelType> >(new CEMParams_Conv<ModelType>());
			break;
		}
	}
	catch (std::bad_alloc& ba) 
	{ 
		std::cerr << "CEM CTOR: bad_alloc caught: " << ba.what() << std::endl; 
	}
};

template <class ModelType>
inline void CEM<ModelType>::EM_Initialization() 
{
	m_EMInitializer->EM_Initialization(m_Data, m_MoG);
}

template <class ModelType>
void CEM<ModelType>::E_Step() 
{
	if (EM_GLOBALS::PRINT_PERFORMANCE_REPORT) 
	{
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.SetMode(MODE_ESTEP);
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.AddIteration();
	}

	// RESET CLUSTER OF POINTS
	size_t nModels = m_MoG->m_Models.size();
	size_t nPoints = m_Data.size();
	for (size_t k = 0; k < nModels; ++k) 
	{
		m_MoG->m_Models[k].ResetCluster(); //clear data and loglikelihood = 0
	}

	// E-STEP -> FIND CLUSTERS AND LIKELIHOODS FOR THE CURRENT PARAMETERS 
	if (EM_GLOBALS::USE_COMPLETE_PDF) 
	{
		double TotalLogL = 0;
		for (size_t n = 0; n < nPoints; ++n) 
		{
			double maxL = -10000000; 
			int modelForthisPoint = -1;
			double posteriorDenom = 0;
			for (int s = 0; s < nModels; ++s) 
			{
				double L = m_MoG->m_Models[s].m_pModel->LikelihoodFunction(m_Data[n]);
				if (L >= maxL) 
				{ 
					maxL = L; 
					modelForthisPoint = s; 
				}
				posteriorDenom += m_MoG->m_Models[s].m_mixtureLambda * L;
			}
			double logTotalForPoint = log(posteriorDenom);
			TotalLogL += logTotalForPoint;
			try 
			{
				if (modelForthisPoint == -1) 
					throw " CEM::E_Step: Point is not being assigned to any model!";
				
				m_MoG->m_Models[modelForthisPoint].m_clusterData.push_back(m_Data[n]);
				m_MoG->m_Models[modelForthisPoint].AddLogLikelihood(logTotalForPoint);
			}
			catch (char* c) 
			{ 
				std::cout << c << std::endl; 
				return; 
			}
		}
		m_MoG->SetLogLikelihood(TotalLogL);
	}
	else 
	{
		double TotalLogL = 0;
		for (size_t n = 0; n < nPoints; ++n) 
		{
			double maxL = -10000000; 
			int modelForthisPoint = -1;
			for (size_t s = 0; s < nModels; ++s) 
			{
				double L = m_MoG->m_Models[s].m_pModel->LogLikelihoodFunction(m_Data[n]);
				if (L >= maxL) 
				{ 
					maxL = L; 
					modelForthisPoint = s; 
				}
			}
			try 
			{
				if (modelForthisPoint == -1) 
					throw " CEM::E_Step: Point is not being assigned to any model!";
				
				m_MoG->m_Models[modelForthisPoint].m_clusterData.push_back(m_Data[n]);
				m_MoG->m_Models[modelForthisPoint].AddLogLikelihood(maxL);
				TotalLogL += maxL;
			}
			catch (char* c) 
			{ 
				std::cout << c << std::endl; 
				return; 
			}
		}
		m_MoG->SetLogLikelihood(TotalLogL);
	}
}

template <class ModelType>
void CEM<ModelType>::M_Step() 
{
	if (EM_GLOBALS::PRINT_PERFORMANCE_REPORT) 
	{
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.SetMode(MODE_MSTEP);
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.AddIteration();
	}

	// NEW PARAMETERS COMPUTATION FROM CURRENT CLUSTERS 
	size_t nModels = m_MoG->m_Models.size();
	for (size_t s = 0; s < nModels; ++s) 
	{
		try 
		{ 
			m_MoG->m_Models[s].m_pModel->FindParamsFromData(m_MoG->m_Models[s].m_clusterData); 
		}
		catch (std::overflow_error e) 
		{ 
			std::cout << " CEMBase::M-STEP: " << e.what() << "on model " << s << std::endl; 
			return; 
		}
	}
}

template <class ModelType>
inline void CEM<ModelType>::EM_Iteration() 
{
	M_Step();
	E_Step();
}

template <class ModelType>
void CEM<ModelType>::EM(size_t NMaxIter) 
{
	try 
	{ 
		if (m_MoG->m_Models.empty()) 
			throw "CEM::EM: m_Models is empty!"; 
	}
	catch (char* c) 
	{ 
		std::cout << c << std::endl; 
		return; 
	}

	// INITIALIZE CLUSTERS
	E_Step();

	// EM MAIN CYCLE
	m_convCriteria->SetMaxIter(NMaxIter);
	do 
	{ 
		this->EM_Iteration(); 
	} 
	while (!m_convCriteria->Iteration(m_MoG));
}

template <class ModelType>
CSMEM<ModelType>::CSMEM(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence, boost::shared_ptr<CSplitMergeCriteria<ModelType> > smc) : CEM<ModelType>(Data, MoG, emInit, convergence), m_SMC(smc)
{
};

template <class ModelType>
void CSMEM<ModelType>::SMEM(size_t NMaxIter) 
{
	// Initial EM
	CEM<ModelType>::EM(NMaxIter);

	// Split Merge EM Cycle
	size_t nIter = 0;
	size_t nComps = m_MoG->m_Models.size();

	boost::shared_ptr<EMConvCriteriaBase<ModelType> > smemConvCriteria(new CEMLOGL_Conv<ModelType>(this->m_Data));
	
	if (EM_GLOBALS::SHOW_PROGRESS) 
		smemConvCriteria->SetImage(this->m_convCriteria->GetImage());

	smemConvCriteria->SetMaxIter(NMaxIter);

	do 
	{
		// SPLIT 
		SplitModels();
		m_MoG->NormalizeLambdas();

		// EM 
		CEM<ModelType>::EM(NMaxIter);

		// MERGE 
		MergeModels();
		m_MoG->NormalizeLambdas();

		// EM 
		CEM<ModelType>::EM(NMaxIter);

		nIter++;
		nComps = m_MoG->m_Models.size();
	} 
	while (!smemConvCriteria->Iteration(m_MoG) && nComps < EM_SMCRITERIA::MaxNumberOfComponents);
}

template <class ModelType>
void CSMEM<ModelType>::SplitModels() 
{
	if (EM_GLOBALS::SHOW_PROGRESS) 
	{
		std::cout << "SPLIT STARTED" << std::endl;
	}
	if (EM_GLOBALS::PRINT_PERFORMANCE_REPORT) 
	{
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.SetMode(MODE_SPLIT);
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.AddIteration();
	}

	std::vector<MixtureModel<ModelType> > newModels;
	size_t nModels = m_MoG->m_Models.size();
	bool needReplacing = false;

	for (size_t s = 0; s<nModels; ++s) 
	{
		try 
		{ 
			if (m_MoG->m_Models[s].m_clusterData.empty())
				throw("CSEM::SplitModels, cluster data is empty!"); 
		}
		catch (char* c) 
		{
			std::cout << c << std::endl;  
			continue; 
		}

		// SPLIT CRITERIA
		Eigen::VectorXd P1(5); Eigen::VectorXd P2(5);
		if (m_SMC->SplitCriteria(m_MoG, s, P1, P2))
		{ // SPLIT HAPPENING ->
			newModels.push_back(MixtureModel<ModelType>(P1, m_MoG->m_Models[s].m_mixtureLambda*0.5));
			newModels.push_back(MixtureModel<ModelType>(P2, m_MoG->m_Models[s].m_mixtureLambda*0.5));
			needReplacing = true;
		}
		else
		{
			newModels.push_back(MixtureModel<ModelType>(m_MoG->m_Models[s].m_pModel->GetParams(), m_MoG->m_Models[s].m_mixtureLambda));
		}
	}

	// Finally, replace the vector of models of this MoG with the vector of new_models
	if (needReplacing)
	{
		m_MoG->m_Models.clear();
		m_MoG->m_Models = newModels;
	}

	if (EM_GLOBALS::SHOW_PROGRESS) 
	{
		std::cout << "SPLIT ENDED" << std::endl;
	}
}

template <class ModelType>
void CSMEM<ModelType>::MergeModels() 
{
	if (EM_GLOBALS::SHOW_PROGRESS) 
	{
		std::cout << "MERGE STARTED" << std::endl;
	}
	if (EM_GLOBALS::PRINT_PERFORMANCE_REPORT) 
	{
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.SetMode(MODE_MERGE);
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.AddIteration();
	}

	std::vector<MixtureModel<ModelType> > newModels;
	std::set<size_t> modelsToReplace;
	size_t nModels = m_MoG->m_Models.size();

	for (size_t s = 0; s < nModels; ++s) 
	{
		if (modelsToReplace.find(s) != modelsToReplace.end()) 
			continue; 

		try 
		{ 
			if (m_MoG->m_Models[s].m_clusterData.empty()) 
				throw("CSEM::MergeModels, cluster data is empty!"); 
		}
		catch (char* c) 
		{ 
			std::cout << c << std::endl;  
			continue; 
		}

		for (size_t l = s + 1; l<nModels; ++l) 
		{
			if (modelsToReplace.find(l) != modelsToReplace.end()) 
				continue;

			try 
			{ 
				if (m_MoG->m_Models[l].m_clusterData.empty()) 
					throw("CSEM::MergeModels, cluster data is empty!"); 
			}
			catch (char* c) 
			{ 
				std::cout << c << std::endl; 
				continue; 
			}

			// MERGE CRITERIA 
			Eigen::VectorXd P(5);
			if (m_SMC->MergeCriteria(m_MoG, s, l, P))
			{ // MERGE HAPPENING -> 
				newModels.push_back(MixtureModel<ModelType>(P, m_MoG->m_Models[s].m_mixtureLambda + m_MoG->m_Models[l].m_mixtureLambda));
				modelsToReplace.insert(s);
				modelsToReplace.insert(l);
			}
		}
	}

	// Finally, replace the vector of models of this MoG with the vector of new_models 
	if (!modelsToReplace.empty()) 
	{
		for (size_t s = 0; s<nModels; ++s) 
		{
			if (modelsToReplace.find(s) == modelsToReplace.end()) 
				newModels.push_back(m_MoG->m_Models[s]);
		}

		m_MoG->m_Models.clear();
		m_MoG->m_Models = newModels;
	}

	if (EM_GLOBALS::SHOW_PROGRESS) 
	{
		std::cout << "MERGE ENDED" << std::endl;
	}
}

template <class ModelType>
CSMEM_MC<ModelType>::CSMEM_MC(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence, boost::shared_ptr<CSplitMergeCriteria<ModelType> > smc) : CSMEM<ModelType>(Data, MoG, emInit, convergence, smc) 
{
};

template <class ModelType>//Using MaxIterations Convergence
void CSMEM_MC<ModelType>::MC_SMEM(const T2dPointVec & Samples, size_t NMaxIter) 
{
	// PERFORM SMEM ON SAMPLES
	boost::shared_ptr<MixtureOfModels<ModelType> > tempM(new MixtureOfModels<ModelType>());
	CSMEM<ModelType> EMinstance(Samples, tempM, this->m_EMInitializer, EM_MAXITER, this->m_SMC);
	EMinstance.EM_Initialization();
	EMinstance.SMEM(NMaxIter);

	// NOW COPY MODELS TO THIS INSTANCE AND PERFORM SMEM ON FULL DATA
	m_MoG->m_Models = tempM->m_Models;
	CSMEM<ModelType>::SMEM(NMaxIter);
}

template <class ModelType>
CSMEM_HEURISTIC<ModelType>::CSMEM_HEURISTIC(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence, boost::shared_ptr<CSplitMergeCriteria<ModelType> > smc) : CSMEM<ModelType>(Data, MoG, emInit, convergence, smc)
{
};

template <class ModelType>
void CSMEM_HEURISTIC<ModelType>::SMEM_HEURISTIC(size_t NMaxIter) 
{
	if (EM_GLOBALS::WRITE_EM_FRAMES) 
		EM_GLOBALS::writeActivated = true;

	// Initial EM 
	CEM<ModelType>::EM(NMaxIter);

	if (EM_GLOBALS::WRITE_EM_FRAMES)
		EM_GLOBALS::writeActivated = false;
	
	// Split - EM Cycle
	size_t nIter = 0
	size_t nComps = m_MoG->m_Models.size();
	boost::shared_ptr<EMConvCriteriaBase<ModelType> > smemConvCriteria(new CEMLOGL_Conv<ModelType>(this->m_Data));
	
	if (EM_GLOBALS::SHOW_PROGRESS) 
		smemConvCriteria->SetImage(this->m_convCriteria->GetImage());
	
	smemConvCriteria->SetMaxIter(NMaxIter);
	
	do 
	{
		// SPLIT
		this->SplitModels();
		m_MoG->NormalizeLambdas();

		if (EM_GLOBALS::WRITE_EM_FRAMES) 
			EM_GLOBALS::writeActivated = true;
		
		// EM 
		CEM<ModelType>::EM(NMaxIter);

		if (EM_GLOBALS::WRITE_EM_FRAMES) 
			EM_GLOBALS::writeActivated = false;
		
		nIter++;
		nComps = m_MoG->m_Models.size();
	} 
	while (!smemConvCriteria->Iteration(m_MoG) && nComps < EM_SMCRITERIA::MaxNumberOfComponents);

	/*FINAL MERGE + EM*/
	//this->MergeModels();
	//CEM<ModelType>::EM(NMaxIter);
}

template class CEM<GaussianModel>;
template class CSMEM<GaussianModel>;
template class CSMEM_MC<GaussianModel>;	
template class CSMEM_HEURISTIC<GaussianModel>;
