//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "EMInitializer.h"
#include "GaussianModel.h"
#include <set>
#include <stdlib.h> 
#include<time.h>

template <class ModelType>
RandomEMInitializer<ModelType>::RandomEMInitializer(int nModels) : m_RandomInitialModels(nModels) 
{
}

template <class ModelType>
void RandomEMInitializer<ModelType>::EM_Initialization(const T2dPointVec & data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG) const 
{
	try 
	{
		if (m_RandomInitialModels == 0) 
			throw "RandomEMInitializer : Instance not initialized";
		else if (data.empty())  
			throw "RandomEMInitializer : data argument is empty!";

		//std::srand (time(NULL));
		double defaultLambda = 1.0 / static_cast<double>(m_RandomInitialModels);
		int k = 0; 
		std::set<int> modelIDs; 
		while (k < m_RandomInitialModels) 
		{
			int rIdx = rand() % data.size();
			if (modelIDs.size() == 0) 
				modelIDs.insert(rIdx);
			else 
			{
				std::set<int>::iterator it = modelIDs.find(rIdx);
				if (it != modelIDs.end()) 
					continue;
			}
			MoG->AddModel(Eigen::Vector2d(data[rIdx](0), data[rIdx](1)), defaultLambda); //col,row
			k++;
		}

		if (EM_GLOBALS::SHOW_PROGRESS) 
		{
			cv::Mat img = UtilityMethods::GetColorBinaryImage(data, 657, 1130);
			for (size_t k = 0; k < MoG->m_Models.size(); ++k) 
			{
				MoG->m_Models[k].m_pModel->RenderModel(img);
			}
			cv::imshow("EM Iteration ", img);
			cv::waitKey(33);
		}
	}
	catch (char* c) 
	{ 
		std::cout << c << std::endl; 
	}
}

template class EMInitializerBase<GaussianModel>;
template class RandomEMInitializer<GaussianModel>;
template class EMInitializerBase<DecayBoxModel>;
template class RandomEMInitializer<DecayBoxModel>;
