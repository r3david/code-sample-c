//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef CEM_H
#define CEM_H

#include <boost/shared_ptr.hpp>
#include "customdefs.h"
#include "EMInitializer.h"
#include "MixtureOfModels.h"
#include "SplitMergeCriteria.h"
#include "EMConvCriteria.h"

// Base Expectation Maximization to fit a mixture of models to the data in m_data
template <class ModelType>
class CEM 
{
public:
	CEM(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence);
	virtual ~CEM() {}
	void EM_Initialization();
	void E_Step();
	void M_Step();
	void EM_Iteration();
	virtual void EM(size_t NMaxIter);
	
	inline void SetImageForShowingProgress(cv::Mat & Image) const 
	{ 
		m_convCriteria->SetImage(Image); 
	}

protected:
	T2dPointVec m_Data;	// data to fit 
	boost::shared_ptr<EMInitializerBase<ModelType> >  m_EMInitializer;
	boost::shared_ptr<EMConvCriteriaBase<ModelType> > m_convCriteria;
	boost::shared_ptr<MixtureOfModels<ModelType> > m_MoG;
};


// Split and Merge version of the EM
template <class ModelType>
class CSMEM : public CEM<ModelType> 
{
public:
	CSMEM(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence, boost::shared_ptr<CSplitMergeCriteria<ModelType> > smc);
	virtual void SMEM(size_t NMaxIter);
	virtual void SplitModels();
	virtual void MergeModels();
protected:
	using CEM<ModelType>::m_MoG;
	boost::shared_ptr<CSplitMergeCriteria<ModelType> > m_SMC;
};


// MONTE CARLO - SPLIT-MERGE EM 
template <class ModelType>
class CSMEM_MC : public CSMEM<ModelType> 
{
public:
	CSMEM_MC(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence, boost::shared_ptr<CSplitMergeCriteria<ModelType> > smc);
	virtual void MC_SMEM(const T2dPointVec & Samples, size_t NMaxIter);

protected:
	using CEM<ModelType>::m_MoG;
	using CSMEM<ModelType>::m_SMC;
};


// HEURISTIC - SPLIT MERGE
template <class ModelType>
class CSMEM_HEURISTIC : public CSMEM<ModelType> 
{
public:
	CSMEM_HEURISTIC(const T2dPointVec & Data, boost::shared_ptr<MixtureOfModels<ModelType> > MoG, boost::shared_ptr<EMInitializerBase<ModelType> > emInit, ConvType convergence, boost::shared_ptr<CSplitMergeCriteria<ModelType> > smc);
	void SMEM_HEURISTIC(size_t NMaxIter);

protected:
	using CEM<ModelType>::m_MoG;
	using CSMEM<ModelType>::m_SMC;
};


#endif