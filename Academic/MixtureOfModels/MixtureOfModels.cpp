//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "MixtureOfModels.h"
#include <stdexcept>
#include <set>
#include <time.h>

template <class ModelType>
inline void MixtureOfModels<ModelType>::AddModel(const Eigen::VectorXd & Params, const double & lambda) 
{
	m_Models.push_back(MixtureModel<ModelType>(Params, lambda));
	m_nParams += static_cast<int>(Params.size());
}

template <class ModelType>
inline void MixtureOfModels<ModelType>::AddModel(const Eigen::Vector2d & midPoint, const double & lambda) 
{
	m_Models.push_back(MixtureModel<ModelType>(midPoint, lambda));
	m_nParams += static_cast<int>(m_Models[static_cast<int>(m_Models.size() - 1)].m_pModel->GetParams().size());
}

template <class ModelType>
inline void MixtureOfModels<ModelType>::AddModel(const MixtureModel<ModelType> & model) 
{
	m_Models.push_back(model);
	m_nParams += static_cast<int>(model.m_pModel->GetParams().size());
}

template <class ModelType>
double MixtureOfModels<ModelType>::MixturePDFAtPoint(const Eigen::Vector2d & p) const 
{
	double pdf_value = 0;
	size_t nModels = m_Models.size();
	for (size_t s = 0; s<nModels; ++s) 
	{
		pdf_value += m_Models[s].m_mixtureLambda * m_Models[s].m_pModel->LikelihoodFunction(p);
	}
	return pdf_value;
}

template <class ModelType>
double MixtureOfModels<ModelType>::GetLogMixturePDFOnData(const T2dPointVec & data) const 
{
	size_t nPoints = data.size();
	double LogL_value = 0;
	for (size_t i = 0; i<nPoints; ++i) 
	{
		LogL_value += log(MixturePDFAtPoint(data[i]));
	}
	return LogL_value;
}

/* declare ahead correct methods instances */
template class MixtureOfModels<GaussianModel>;
template class MixtureOfModels<DecayBoxModel>;
