//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef ABS_MODEL_H
#define ABS_MODEL_H

#include "Eigen/Core"
#include "CustomDefs.h"

enum pointType 
{ 
	P_EXTA = 0,
	P_EXTB = 1, 
	P_MIDDLE = 2 
};

class AbstractSegmentModel 
{
protected:
	Eigen::VectorXd m_params; 

public:
	inline Eigen::VectorXd GetParams() const
	{
		return m_params;
	};

	inline virtual void SetParams(const Eigen::VectorXd & params) 
	{
		m_params = params;
	};

	virtual double LikelihoodFunction(const Eigen::Vector2d & x) const = 0;

	virtual double LogLikelihoodFunction(const Eigen::Vector2d & x) const 
	{
		return log(LikelihoodFunction(x));
	};

	virtual void FindParamsFromData(const T2dPointVec & data) = 0;

	virtual double LogLikelihoodForOptimization(const Eigen::VectorXd & params, const Eigen::Vector2d & x) const 
	{
		return 0;
	};

	virtual void RenderModel(cv::Mat & img) const = 0;

	virtual void GetModelPoints(const T2dPointVec & dataCluster, T2dPointVec & points) = 0;

protected:
	AbstractSegmentModel() 
	{
	};

	AbstractSegmentModel(const Eigen::VectorXd & params) : m_params(params) 
	{
	};

	virtual ~AbstractSegmentModel() 
	{
	};
};

#endif 
