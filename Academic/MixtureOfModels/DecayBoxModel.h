//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//----------------------------------------------------------------------------- 

#ifndef DBOX_MODEL_H
#define DBOX_MODEL_H

#include "Eigen/Core"
#include <opencv2/opencv.hpp>
#include "AbstractSegmentModel.h"
#include "OptimizationFunction.h" 
#ifndef __GNUC__
//#error " need to add this header to svn"
#endif


#define BOX_MODEL_SGM 2.0
#define OPT_NITER 1000

class DecayBoxModel : public AbstractSegmentModel 
{
public:
	~DecayBoxModel();
	DecayBoxModel(const Eigen::VectorXd & params);
	DecayBoxModel(const Eigen::Vector2d & middlePoint);

	/* INTERFACE IMPLEMENTATION */
	inline virtual void SetParams(const Eigen::VectorXd & params);
	double LikelihoodFunction(const Eigen::Vector2d & x) const;
	void FindParamsFromData(const T2dPointVec & data);
	virtual double LogLikelihoodForOptimization(const Eigen::VectorXd & params, const Eigen::Vector2d & x) const;
	void RenderModel(cv::Mat & img) const;
	void GetModelPoints(const T2dPointVec & dataCluster, T2dPointVec & points);

	/* OTHER METHODS */
	void GenericParametersAtPoint(const Eigen::Vector2d & middlePoint);
	inline Eigen::Vector2d GetProjectionToSegmentLine(const Eigen::Vector2d & x) const;
	void GetExtremePoints(Eigen::Vector2d & A, Eigen::Vector2d & B) const;

private:
	void PrecomputeConstants();

private:
	Eigen::Vector2d m_extA, m_extB;
	double m_segmentWidth;
	Eigen::Vector2d m_segmentDir;
	double m_segmentLength;
	double m_NormalizingFactor;
};

#endif