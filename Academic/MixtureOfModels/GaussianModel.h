//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef GAUSSIAN_MODEL_H
#define GAUSSIAN_MODEL_H

#include "Eigen/Core"
#include <opencv2/opencv.hpp>
#include "UtilityMethods.h"
#include "AbstractSegmentModel.h"

/* Parameters of the model are: sigma11,sigma22, sigma12, meanCol, meanRow (0,1,2,3,4) */
class GaussianModel : public AbstractSegmentModel 
{
public:
	virtual ~GaussianModel();
	GaussianModel(const Eigen::VectorXd & params);
	GaussianModel(const Eigen::Vector2d & middlePoint);

	/* INTERFACE IMPLEMENTATION */
	inline virtual void SetParams(const Eigen::VectorXd & params);
	double LikelihoodFunction(const Eigen::Vector2d & x) const;
	double LogLikelihoodFunction(const Eigen::Vector2d & x) const;
	void FindParamsFromData(const T2dPointVec & data);
	void RenderModel(cv::Mat & img) const;
	void GetModelPoints(const T2dPointVec & dataCluster, T2dPointVec & points);

	/* OTHER METHODS */
	void GenericParametersAtPoint(const Eigen::Vector2d & middlePoint);
	void ComputeExtremePoints_EigenVectors(Eigen::Vector2d  & extA, Eigen::Vector2d  & extB); //Eigenvectors & Eigenvalues
	void ComputeExtremePoints_EigenHeuristic(const cv::Mat & MapData, Eigen::Vector2d  & extA, Eigen::Vector2d  & extB); //Eigenvectors & Eigenvalues combined with a Heuristic approach
	void ComputeExtremePoints_LRFit(const T2dPointVec & data, Eigen::Vector2d  & extA, Eigen::Vector2d  & extB); //Linear Regression
	void GetExtremePoints(Eigen::Vector2d  & extA, Eigen::Vector2d  & extB) const;
	void GetMeanPoint(Eigen::Vector2d & mean) const;
	double AngleToModel(const GaussianModel & other) const; //computes the angle in radians between this model and the argument.	
	double GetVariance()const;

private:
	void PrecomputeConstants();

private:
	Eigen::Matrix2d m_SIGMA;
	Eigen::Matrix2d m_SIGMA_Inv;
	Eigen::Vector2d m_mean;
	double m_factor;

	Eigen::Vector2d m_extA;
	Eigen::Vector2d m_extB;
};

#endif
