//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MIX_MODELS_H
#define MIX_MODELS_H

#include <Eigen/Core>
#include <Eigen/StdVector>
#include "CustomDefs.h"
#include "MixtureModel.h"

template <class ModelType>
class MixtureOfModels 
{
public:
	MixtureOfModels() 
	{
		m_nParams = 0;
		m_TotalLogL = 0;
	};

	~MixtureOfModels() 
	{
		m_Models.clear();
	};

	/* Add a single model with parameters as the arguments */
	void AddModel(const Eigen::VectorXd & Params, const double & lambda);

	/* Add a single model at a point (with default params), and lambda passed as arguments */
	void AddModel(const Eigen::Vector2d & midPoint, const double & lambda);

	/* Add a model */
	void AddModel(const MixtureModel<ModelType> & model);

	inline int GetNumberOfParams() const 
	{
		return m_nParams;
	}

	inline size_t GetNumberOfParamsForModel(size_t modelIdx) const 
	{
		return m_Models[modelIdx].m_pModel->GetParams().size();
	}

	/* The Total Likelihood of this set of mixtures (CURRENTLY IS NOT SCALED BY THE MIXTURE COEFFICIENTS)  */
	inline void SetLogLikelihood(double L) 
	{
		m_TotalLogL = L;
	}

	/* Returns the precomputed value of the total log likelihood */
	inline double GetTotalLogLikelihood() const 
	{
		return m_TotalLogL;
	}

	/* computes the complete pdf of the muixture models at point p */
	double MixturePDFAtPoint(const Eigen::Vector2d & p) const;

	/* uses MixturePDFAtPoint and all the data, and return the sum of the log values*/
	double GetLogMixturePDFOnData(const T2dPointVec & data) const;

	inline void NormalizeLambdas() 
	{
		const double lambda = 1.0 / static_cast<double>(m_Models.size());
		for (size_t i = 0; i < m_Models.size(); ++i) 
		{
			m_Models[i].m_mixtureLambda = lambda;
		}
	};

protected:
	std::vector<MixtureModel<ModelType> > m_Models;

private:
	int m_nParams;
	double m_TotalLogL;
};

#endif