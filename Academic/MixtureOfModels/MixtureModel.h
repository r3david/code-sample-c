//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MIX_MODEL_H
#define MIX_MODEL_H

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <boost/shared_ptr.hpp>
#include "CustomDefs.h"
#include "GaussianModel.h"
#include "DecayBoxModel.h"

//Template class for a single model including the mixture coefficient lambda:   lamda*ModelType(x) 
//ModelType should implement the "AbstractSegmentModel"
template <class ModelType>
class MixtureModel 
{

public:
	//Create a Model using the parameters and the lambda value d
	MixtureModel(const Eigen::VectorXd & params, const double & d) 
	{
		try 
		{ 
			m_pModel = boost::shared_ptr<ModelType>(new ModelType(params)); 
		}
		catch (std::bad_alloc& ba) 
		{ 
			std::cerr << "MixtureModel: bad_alloc caught: " << ba.what() << std::endl; 
		}

		m_mixtureLambda = d;
		m_ClusterLogL = 0;
	}

	//Create a Model using a middle point to compute default parameters and the lambda value d
	MixtureModel(const Eigen::Vector2d & middlePoint, const double & d) 
	{
		try 
		{
			m_pModel = boost::shared_ptr<ModelType>(new ModelType(middlePoint)); 
		}
		catch (std::bad_alloc& ba) 
		{ 
			std::cerr << "MixtureModel: bad_alloc caught: " << ba.what() << std::endl; 
		}

		m_mixtureLambda = d;
		m_ClusterLogL = 0;
	}

	~MixtureModel() 
	{ 
	}

	inline void ResetCluster() 
	{
		m_clusterData.clear();
		m_ClusterLogL = 0;
	}

	inline void AddLogLikelihood(double logL) 
	{
		m_ClusterLogL += logL;
	}

	inline double GetLogLikelihood() const 
	{
		return m_ClusterLogL;
	}

protected:
	boost::shared_ptr<ModelType> m_pModel; // model
	double m_mixtureLambda; // mixture coefficient 
	T2dPointVec m_clusterData; // data that is currently assigned to this model

private:
	double m_ClusterLogL;// The LogLikelihood of the cluster data for this model

};

template class MixtureModel<GaussianModel>;

#endif