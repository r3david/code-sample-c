//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "GaussianModel.h"
#include <Eigen/StdVector>
#include <Eigen/Dense>
#include "CustomDefs.h"
#include <stdexcept> 

GaussianModel::~GaussianModel(void)
{
}

GaussianModel::GaussianModel(const Eigen::VectorXd & params)
{
	m_extA = Eigen::Vector2d(-1, -1);
	m_extB = Eigen::Vector2d(-1, -1);
	SetParams(params);
}

GaussianModel::GaussianModel(const Eigen::Vector2d & middlePoint)
{
	m_extA = Eigen::Vector2d(-1, -1);
	m_extB = Eigen::Vector2d(-1, -1);
	GenericParametersAtPoint(middlePoint);
};

inline void GaussianModel::SetParams(const Eigen::VectorXd & params)
{
	m_extA(0) = -1;
	m_extA(1) = -1;//set the extreme points to unitialized, so if we use GetExtremePoints we throw an error, since we must use first any of the computeExtremePoints methods first.
	m_params = params;
	PrecomputeConstants();
}

double GaussianModel::LikelihoodFunction(const Eigen::Vector2d & x) const
{
	Eigen::Vector2d x_minus_mean = x - m_mean;
	double exponentialPower = (m_SIGMA_Inv*x_minus_mean).dot(x_minus_mean);
	double result = 1.0 / m_factor * exp(exponentialPower);
	return result;
}

double GaussianModel::LogLikelihoodFunction(const Eigen::Vector2d & x) const
{
	if (EM_GLOBALS::PRINT_PERFORMANCE_REPORT)
		EM_GLOBALS::GLOBAL_PERFORMANCE_LOG.AddLogLCall();

	Eigen::Vector2d x_minus_mean = x - m_mean;
	double exponentialPower = (m_SIGMA_Inv*x_minus_mean).dot(x_minus_mean);
	double result = -log(m_factor) + exponentialPower;
	return result;
}

void GaussianModel::FindParamsFromData(const T2dPointVec & data)
{
	size_t nsize = data.size();
	try
	{
		if (nsize == 0)
			throw std::overflow_error("GaussianModel::FindParamsFromData: CLUSTER WITH NO POINTS! Params not updated");
	}
	catch (std::overflow_error e)
	{
		std::cout << e.what() << std::endl;
		return;
	}

	// NEW MEAN
	Eigen::Vector2d newMean(0, 0);
	for (size_t j = 0; j < nsize; ++j)
	{
		newMean(0) += data[j](0);
		newMean(1) += data[j](1);
	}
	newMean /= nsize;

	// 2. NEW COVARIANCE MATRIX 
	Eigen::Matrix2d newSIGMA; newSIGMA << 0, 0, 0, 0;
	for (size_t j = 0; j < nsize; ++j)
	{
		Eigen::Vector2d vec = (data[j] - newMean);

		newSIGMA(0, 0) += vec(0)*vec(0);
		newSIGMA(1, 0) += vec(0)*vec(1);
		newSIGMA(0, 1) += vec(1)*vec(0);
		newSIGMA(1, 1) += vec(1)*vec(1);

	}
	if (nsize > 1)
		newSIGMA *= 1.0 / ((double)nsize - 1.0);

	// SET NEW PARAMS
	Eigen::VectorXd newParams(5);
	newParams(0) = newSIGMA(0, 0);
	newParams(1) = newSIGMA(1, 1);
	newParams(2) = newSIGMA(1, 0);
	newParams(3) = newMean(0);
	newParams(4) = newMean(1);
	SetParams(newParams);
}

void GaussianModel::RenderModel(cv::Mat & img) const
{
	const int radius = 2;
	const int thickness = 2;

	cv::Point2d Mean;
	UtilityMethods::EigenToCvPoint_2dim(m_mean, Mean);

	/*DRAW ELLIPSE HERE*/
	UtilityMethods::drawEllipse(img, m_SIGMA, m_mean, radius, cv::Scalar(1, 0, 0), thickness, true);

	/*DRAW EXTREME POINTS*/
	//cv::circle(img,cv::Point2d(m_extA(0),m_extA(1)),5,cv::Scalar(0,0,1),2);
	//cv::circle(img,cv::Point2d(m_extB(0),m_extB(1)),5,cv::Scalar(0,0,1),2);
	//cv::line(img,cv::Point2d(m_extA(0),m_extA(1)),cv::Point2d(m_extB(0),m_extB(1)),cv::Scalar(0,0,1),2);
}

void GaussianModel::GetModelPoints(const T2dPointVec & dataCluster, T2dPointVec & points)
{
	points.clear();
	points.push_back(Eigen::Vector2d::Zero());
	points.push_back(Eigen::Vector2d::Zero());
	points.push_back(Eigen::Vector2d::Zero());

	cv::Mat Map;
	UtilityMethods::GetBinaryImage(dataCluster, 2000, 2000, Map);//USING IMAGES OF MAXIMUM 2000 rows x 2000 cols
	ComputeExtremePoints_EigenHeuristic(Map, m_extA, m_extB);
	//ComputeExtremePoints_EigenVectors(m_extA,m_extB);

	GetExtremePoints(points[P_EXTA], points[P_EXTB]);
	GetMeanPoint(points[P_MIDDLE]);
}

void GaussianModel::GenericParametersAtPoint(const Eigen::Vector2d & middlePoint)
{
	double col = middlePoint(0);
	double row = middlePoint(1);

	double diffVer = 8;
	double diffHor = 0;

	m_params = Eigen::VectorXd(5);
	if (fabs(diffVer - diffHor)<0.01)
	{
		m_params(0) = 0.5;
		m_params(1) = 0.5;
	}
	else if (diffVer > diffHor)
	{
		m_params(0) = 0.1;
		m_params(1) = 0.6;
	}
	else
	{
		m_params(0) = 0.6;
		m_params(1) = 0.1;
	}
	m_params(2) = 0;  m_params(3) = col; m_params(4) = row;
	PrecomputeConstants();
}

void GaussianModel::ComputeExtremePoints_EigenVectors(Eigen::Vector2d  & extA, Eigen::Vector2d  & extB)
{
	Eigen::EigenSolver<Eigen::Matrix2d> eigenVV(m_SIGMA);
	const Eigen::Vector2d & eigvals = eigenVV.eigenvalues().real();
	const Eigen::Matrix2d & eigVectors = eigenVV.eigenvectors().real();

	double d0 = sqrt(eigvals(0));
	double d1 = sqrt(eigvals(1));
	double d; Eigen::Vector2d e(0, 0);
	if (d0>d1)
	{
		e = Eigen::Vector2d(eigVectors(0, 0), eigVectors(1, 0));
		d = d0;
	}
	else
	{
		e = Eigen::Vector2d(eigVectors(0, 1), eigVectors(1, 1));
		d = d1;
	}

	m_extA = m_mean + e*d;
	m_extB = m_mean - e*d;

	extA = m_extA;
	extB = m_extB;
}

void GaussianModel::ComputeExtremePoints_EigenHeuristic(const cv::Mat & MapData, Eigen::Vector2d  & extA, Eigen::Vector2d  & extB)
{
	Eigen::EigenSolver<Eigen::Matrix2d> eigenVV(m_SIGMA);
	const Eigen::Vector2d & eigvals = eigenVV.eigenvalues().real();
	const Eigen::Matrix2d & eigVectors = eigenVV.eigenvectors().real();

	double d0 = sqrt(eigvals(0));
	double d1 = sqrt(eigvals(1));
	double d; Eigen::Vector2d e(0, 0);
	if (d0>d1)
	{
		e = Eigen::Vector2d(eigVectors(0, 0), eigVectors(1, 0));
		d = d0;
	}
	else
	{
		e = Eigen::Vector2d(eigVectors(0, 1), eigVectors(1, 1));
		d = d1;
	}

	// HEURISTICS STARTS HERE -> search data points on the eigenvector line
	Eigen::Vector2d a = m_mean + e*d;
	Eigen::Vector2d b = m_mean - e*d;
	double t = d;
	while (MapData.at<float>(cvRound(a(1)), cvRound(a(0))) == 1)
	{
		t += 0.5f;
		a = m_mean + e*t;
		if (cvRound(a(1))<0 || cvRound(a(1)) >= MapData.rows || cvRound(a(0))<0 || cvRound(a(0)) >= MapData.cols)
			break;
	}

	t = d;
	while (MapData.at<float>(cvRound(b(1)), cvRound(b(0))) == 1)
	{
		t += 0.1f;
		b = m_mean - e*t;
		if (cvRound(b(1))<0 || cvRound(b(1)) >= MapData.rows || cvRound(b(0))<0 || cvRound(b(0)) >= MapData.cols)
			break;
	}
	m_extA = a;
	m_extB = b;
	extA = m_extA;
	extB = m_extB;
}

void GaussianModel::ComputeExtremePoints_LRFit(const T2dPointVec & data, Eigen::Vector2d  & extA, Eigen::Vector2d  & extB)
{
	Eigen::Vector2d a(0, 0), b(0, 0);
	ComputeExtremePoints_EigenVectors(a, b);
	const int dim = (abs(a(0) - b(0)) > abs(a(1) - b(1)) ? 0 : 1); //diffCols > diffRows use
	Eigen::Vector2d lineParams(0, 0);
	UtilityMethods::LinearRegression(data, dim, lineParams);

	/******************* DEBUG ******************
	cv::Mat debugImg = cv::Mat::zeros(328,287,CV_32FC3);
	for(int i=0; i<data.size(); i++){
	debugImg.at<cv::Vec3f>(cvRound(data[i](1)),cvRound(data[i](0))) = cv::Vec3f(1,1,1);
	}
	cv::circle(debugImg,cv::Point2d(a(0),a(1)),5,cv::Scalar(0,0,1),2);
	cv::circle(debugImg,cv::Point2d(b(0),b(1)),5,cv::Scalar(0,0,1),2);
	cv::line(debugImg,cv::Point2d(a(0),a(1)),cv::Point2d(b(0),b(1)),cv::Scalar(0,0,1),2);
	cv::imshow(UtilityMethods::CharConcatenate("LINE FIT ",dim),debugImg); cv::waitKey(0);

	if(dim==0){
	for(int col=0; col<debugImg.cols; col++){
	int e = cvRound(lineParams.dot(Eigen::Vector2d(1.0,(double)col)));
	if(e<0 || e>=debugImg.rows) continue;
	debugImg.at<cv::Vec3f>(e,col) = cv::Vec3f(0,0,1);
	}
	cv::imshow(UtilityMethods::CharConcatenate("LINE FIT ",dim),debugImg);cv::waitKey(0);
	}
	else if(dim==1){
	for(int row=0; row<debugImg.rows; row++){
	int e = cvRound(lineParams.dot(Eigen::Vector2d(1.0,(double)row)));
	if(e<0 || e>=debugImg.cols) continue;
	debugImg.at<cv::Vec3f>(row,e) = cv::Vec3f(0,0,1);
	}
	cv::imshow(UtilityMethods::CharConcatenate("LINE FIT ",dim),debugImg);cv::waitKey(0);
	}
	********** DEBUG **********************/

	int minRowColVal = 100000, maxRowColVal = -1;
	for (size_t i = 0; i < data.size(); ++i)
	{
		if (dim == 0)
		{
			int valCol = cvRound(data[i](0));
			if (valCol<minRowColVal) minRowColVal = valCol;
			if (valCol>maxRowColVal) maxRowColVal = valCol;
		}
		else if (dim == 1) {
			int rowCol = cvRound(data[i](1));
			if (rowCol<minRowColVal) minRowColVal = rowCol;
			if (rowCol>maxRowColVal) maxRowColVal = rowCol;
		}
	}
	if (dim == 0) {
		m_extA(0) = minRowColVal;
		m_extA(1) = cvRound(lineParams.dot(Eigen::Vector2d(1.0, (double)minRowColVal)));
		m_extB(0) = maxRowColVal;
		m_extB(1) = cvRound(lineParams.dot(Eigen::Vector2d(1.0, (double)maxRowColVal)));
	}
	else if (dim == 1) {
		m_extA(0) = cvRound(lineParams.dot(Eigen::Vector2d(1.0, (double)minRowColVal)));
		m_extA(1) = minRowColVal;
		m_extB(0) = cvRound(lineParams.dot(Eigen::Vector2d(1.0, (double)maxRowColVal)));
		m_extB(1) = maxRowColVal;
	}
	extA = m_extA;
	extB = m_extB;
}

void GaussianModel::GetExtremePoints(Eigen::Vector2d  & extA, Eigen::Vector2d  & extB) const
{
	try
	{
		if (m_extA.x() == -1 || m_extA.y() == -1)
			throw "Extreme Points used without being initialized, use any ComputeExtremePoints method first ";

		if (m_extB.x() == -1 || m_extB.y() == -1)
			throw "Extreme Points used without being initialized, use any ComputeExtremePoints method first ";

		extA = m_extA;	extB = m_extB;
	}
	catch (char* e)
	{
		std::cout << "GaussianModel::GetExtremePoints Exception: " << e << std::endl;
	}
}

void GaussianModel::GetMeanPoint(Eigen::Vector2d & mean) const
{
	try
	{
		mean = m_mean;
	}
	catch (...)
	{
		std::cout << "GaussianModel::GetMeanPoint Exception!" << std::endl;
	}
}

double GaussianModel::AngleToModel(const GaussianModel & other) const
{
	double angleout = 0;
	try
	{
		if (m_extA.x() == -1 || m_extA.y() == -1)
			throw "Extreme Points used without being initialized, use any ComputeExtremePoints method first ";

		if (m_extB.x() == -1 || m_extB.y() == -1)
			throw "Extreme Points used without being initialized, use any ComputeExtremePoints method first ";

		Eigen::Vector2d extA, extB;
		other.GetExtremePoints(extA, extB);

		Eigen::Vector2d thisDir = (m_extA - m_extB).normalized();
		Eigen::Vector2d otherDir = (extA - extB).normalized();
		angleout = acos(thisDir.dot(otherDir));
	}
	catch (char* e)
	{
		std::cout << "GaussianModel::GetExtremePoints Exception: " << e << std::endl;
	}

	return angleout;
}

double GaussianModel::GetVariance() const
{
	Eigen::EigenSolver<Eigen::Matrix2d> eigenVV(m_SIGMA);
	const Eigen::Vector2d & eigvals = eigenVV.eigenvalues().real();

	double d0 = sqrt(eigvals(0));
	double d1 = sqrt(eigvals(1));
	return d0>d1 ? d0 : d1;
}

void GaussianModel::PrecomputeConstants()
{
	m_mean = Eigen::Vector2d(m_params(3), m_params(4));
	m_SIGMA << m_params(0), m_params(2), m_params(2), m_params(1);
	m_SIGMA_Inv = -0.5*m_SIGMA.inverse();
	m_factor = 2.0*CV_PI*sqrt(m_SIGMA.determinant());
}
