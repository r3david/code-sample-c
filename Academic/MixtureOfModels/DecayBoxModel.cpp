//-----------------------------------------------------------------------------
// Copyright (c) 2012 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "DecayBoxModel.h"
#include "UtilityMethods.h"
#include <stdexcept> 
#include "OptimizationFunction.h"
#include "geom/levMarNumerical.h"

DecayBoxModel::~DecayBoxModel(void)
{
}

DecayBoxModel::DecayBoxModel(const Eigen::VectorXd & params) 
{
	SetParams(params);
}

DecayBoxModel::DecayBoxModel(const Eigen::Vector2d & middlePoint) 
{
	GenericParametersAtPoint(middlePoint);
};

inline void DecayBoxModel::SetParams(const Eigen::VectorXd & params) 
{
	m_params = params;
	PrecomputeConstants();
}

double DecayBoxModel::LikelihoodFunction(const Eigen::Vector2d & x) const
{
	// 1. COMPUTE CLOSEST POINT TO SEGMENT
	Eigen::Vector2d v2dClosestP = GetProjectionToSegmentLine(x);

	// 2. DISTANCE TO SEGMENT
	double distanceToSegment = (v2dClosestP - x).norm();

	// 3. Likelihood
	if (distanceToSegment <= m_segmentWidth*0.5)
	{ 
		return 1.0 / m_NormalizingFactor; 
	}
	else 
	{
		double expFactor = exp(-0.5*pow(distanceToSegment - m_segmentWidth*0.5, 2.0) / (BOX_MODEL_SGM*BOX_MODEL_SGM));
		double denominator = m_NormalizingFactor*sqrt(2.0*CV_PI)*BOX_MODEL_SGM;
		return 1.0 / denominator*expFactor;
	}
}

void DecayBoxModel::FindParamsFromData(const T2dPointVec & data) 
{
	try 
	{ 
		if (data.empty())
			throw std::overflow_error("DecayBoxModel::FindParamsFromData: CLUSTER WITH NO POINTS! Params not updated");
	}
	catch (std::overflow_error e) 
	{ 
		std::cout << e.what() << std::endl; 
		return; 
	}

	OptimizationFunction optF(this, data);
	CLevMar LM(optF);
	LM.minimise(m_params, OPT_NITER); //m_params will have the optimum params
	PrecomputeConstants();
}

double DecayBoxModel::LogLikelihoodForOptimization(const Eigen::VectorXd & params, const Eigen::Vector2d & x) const 
{
	// 0. CONST FROM ARG PARAMS 
	Eigen::Vector2d extA = Eigen::Vector2d(params(0), params(1));
	Eigen::Vector2d extB = Eigen::Vector2d(params(2), params(3));
	double segmentWidth = params(4);
	double segmentLength = (extB - extA).norm();
	Eigen::Vector2d segmentDir = (extB - extA).normalized();

	//AREA OF RECTANGULAR REGION +  half gaussian integral (0.5) * PERIMETER OF RECTANGULAR REGION 
	//double NormalizingFactor = segmentLength*segmentWidth +  0.5*(2*segmentLength + 2*segmentWidth);
	double NormalizingFactor = segmentLength*segmentWidth + segmentWidth*segmentWidth*0.25*CV_PI + 0.5*(CV_PI*segmentWidth + 2.0*segmentLength);


	// 1. COMPUTE CLOSEST POINT TO SEGMENT
	Eigen::Vector2d v2dClosestP(0, 0);

	Eigen::Vector2d c = x - extA;
	double t = segmentDir.dot(c);

	if (t <= 0)
		v2dClosestP = Eigen::Vector2d(extA);
	else if (t >= segmentLength) 
		v2dClosestP = Eigen::Vector2d(extB);
	else 
		v2dClosestP = extA + segmentDir*t;

	// 2. DISTANCE TO SEGMENT
	double distanceToSegment = (v2dClosestP - x).norm();

	// 3. Likelihood
	if (distanceToSegment <= segmentWidth*0.5) 
		return -log(NormalizingFactor);
	else 
	{
		double LogexpFactor = -0.5*pow(distanceToSegment - segmentWidth*0.5, 2.0) / (BOX_MODEL_SGM*BOX_MODEL_SGM);
		double LogOfNormalizingFactor = -log(NormalizingFactor*sqrt(2.0*CV_PI)*BOX_MODEL_SGM);
		return LogOfNormalizingFactor + LogexpFactor;
	}
}

void DecayBoxModel::RenderModel(cv::Mat & img) const 
{
	Eigen::Vector2d M = (m_extA + m_extB)*0.5;
	Eigen::Vector2d vP(m_segmentDir(1), -m_segmentDir(0));
	Eigen::Vector2d Cd = M + vP*m_segmentWidth*0.5;
	Eigen::Vector2d Dd = M - vP*m_segmentWidth*0.5;

	cv::Point2d A, B, C, D;
	UtilityMethods::EigenToCvPoint_2dim(m_extA, A);
	UtilityMethods::EigenToCvPoint_2dim(m_extB, B);
	UtilityMethods::EigenToCvPoint_2dim(Cd, C);
	UtilityMethods::EigenToCvPoint_2dim(Dd, D);

	int radius = 2;
	int thickness = 2;

	cv::circle(img, A, radius, cv::Scalar(1, 0, 0), thickness, CV_FILLED);
	cv::circle(img, B, radius, cv::Scalar(1, 0, 0), thickness, CV_FILLED);
	cv::line(img, A, B, cv::Scalar(0, 0, 1), thickness, CV_FILLED);

	cv::circle(img, C, radius, cv::Scalar(0, 1, 1), thickness, CV_FILLED);
	cv::circle(img, D, radius, cv::Scalar(0, 1, 1), thickness, CV_FILLED);
	cv::line(img, C, D, cv::Scalar(0, 0, 1), thickness, CV_FILLED);
}

void DecayBoxModel::GetModelPoints(const T2dPointVec & dataCluster, T2dPointVec & points) 
{
	points.clear();
	points.push_back(Eigen::Vector2d::Zero());
	points.push_back(Eigen::Vector2d::Zero());
	points.push_back(Eigen::Vector2d::Zero());

	points[P_EXTA] = m_extA;
	points[P_EXTB] = m_extB;
	points[P_MIDDLE] = 0.5*(m_extA + m_extB);
}

void DecayBoxModel::GenericParametersAtPoint(const Eigen::Vector2d & middlePoint) 
{
	double col = middlePoint(0);
	double row = middlePoint(1);
	double width = 10;

	m_params = Eigen::VectorXd(5);
	m_params(0) = col;  m_params(1) = row + 5;
	m_params(2) = col;  m_params(3) = row - 5;
	m_params(4) = width;

	PrecomputeConstants();
}

inline Eigen::Vector2d DecayBoxModel::GetProjectionToSegmentLine(const Eigen::Vector2d & x)const 
{
	Eigen::Vector2d c = x - m_extA;
	double t = m_segmentDir.dot(c);

	if (t <= 0) 
		return Eigen::Vector2d(m_extA);
	if (t >= m_segmentLength) 
		return Eigen::Vector2d(m_extB);

	return  m_extA + m_segmentDir*t;
}

void DecayBoxModel::PrecomputeConstants() 
{
	m_extA = Eigen::Vector2d(m_params(0), m_params(1));
	m_extB = Eigen::Vector2d(m_params(2), m_params(3));
	m_segmentWidth = m_params(4);
	m_segmentLength = (m_extB - m_extA).norm();
	m_segmentDir = (m_extB - m_extA).normalized();

	//AREA OF RECTANGULAR REGION +  half gaussian integral (0.5) * PERIMETER OF RECTANGULAR REGION 
	//m_NormalizingFactor = m_segmentLength*m_segmentWidth +  0.5*(2*m_segmentLength + 2*m_segmentWidth); 
	m_NormalizingFactor = m_segmentLength*m_segmentWidth + m_segmentWidth*m_segmentWidth*0.25*CV_PI + 0.5*(CV_PI*m_segmentWidth + 2.0*m_segmentLength);
}

void DecayBoxModel::GetExtremePoints(Eigen::Vector2d & A, Eigen::Vector2d & B) const 
{
	A = m_extA;
	B = m_extB;
}