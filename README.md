Copyright (c) 2015 Ricardo David CM - http://ricardo-david.com  

### CONTENT 

This repository contains samples of C++ code. At the moment it is composed of:

-Academic sample code including: Expectation Maximization, Mixture of Models, Simulated Annealing and in superpixels.
The superpixels code can be run in your machine, but you need to link OpenCV 2.x for it to work.

-Samples of an implementation of Behaviour Trees

-Samples of an implementation of Steering Behaviours

-Samples of an implementation of a Finite State Machine for controlling a camera in a 2.5D game.

---------------------------------------------------------------