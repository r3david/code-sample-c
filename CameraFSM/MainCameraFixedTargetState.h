//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MAIN_CAM_FIXED_TARGET_H
#define MAIN_CAM_FIXED_TARGET_H

#include "MainCameraState.h"

//CamFixedTargetModeConfig:
//these are the configurations for the fixed target following mode of the camera
//these derive from the CameraStateEnterParams so they are set when entering the FIXED_TARGET camera state
struct CamFixedTargetModeConfig 
{
	Math::Vector3D targetPosition;
	Math::Float32 speedOfFollow;
};

//MainCameraFixedTargetState:
//implements the state of the main camera that follows a fixed target.
//targets are specified with the structure CamFixedTargetModeConfig
class MainCameraFixedTargetState : public MainCameraState 
{
protected:
	//these is the current configuration set on this state of the camera for following a fixed target
	//it specifies the target position to follow and the speed for the interpolation
	CamFixedTargetModeConfig m_FollowConfig;

public:
	//CTOR
	MainCameraFixedTargetState(Camera* camRef) : MainCameraState(CameraStateMode::FIXED_TARGET, camRef) {};

	//called from the FSM when entering this camera state
	void OnCamStateEnter() override;

	//called from the FSM when exiting this camera state
	void OnCamStateExit() override;

	//called for camera updates from the FSM. 
	//it is called after all rendering is done to update camera properties for the next frame 
	void OnPostRenderUpdate(Math::Float32 time_delta) override;

	//sets the follow settings (fixed target and velocity of follow)
	inline void SetFollowConfiguration(const CamFixedTargetModeConfig & config) 
	{
		m_FollowConfig = config;
	}

};

#endif
