//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MAIN_CAM_NULL_TARGET_H
#define MAIN_CAM_NULL_TARGET_H

#include "MainCameraState.h"

//MainCameraNullTargetState:
//This state does nothing, it is only used to implement the NULL OBJECT pattern on the FSM
class MainCameraNullTargetState : public MainCameraState 
{
public:
	//CTOR: this is a null target state, so we dont store the camera ref, and set it to null ptr
	MainCameraNullTargetState() : MainCameraState(CameraStateMode::NULL_TARGET, nullptr) { };

	//called from the FSM when entering this camera state
	void OnCamStateEnter() override { };

	//called from the FSM when exiting this camera state
	void OnCamStateExit() override { };

	//called for camera updates from the FSM. 
	//it is called after all rendering is done to update camera properties for the next frame 
	void OnPostRenderUpdate(Math::Float32 time_delta) override { };

};

#endif
