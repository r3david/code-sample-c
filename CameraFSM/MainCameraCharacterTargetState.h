//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MAIN_CAM_PLAYABLE_CHARACTER_H
#define MAIN_CAM_PLAYABLE_CHARACTER_H

#include "MainCameraState.h"
#include "PlayableCharacter.h"

//MainCameraCharacterTargetState:
//implements the state of the main camera that follows a playable character
//this implements a 2.5D camera on the XY plane looking to positive Z axis
class MainCameraCharacterTargetState : public MainCameraState 
{
private:
	//the current character the camera is targetting
	//the character also has camera settings 
	//depending on each character settings are designed on the engine editor
	PlayableCharacter* m_character;

	//this is the current camera target, which is a floating point around the character
	//we use it for smooth following the player around
	Math::Vector3D m_camCurrentTargetPos;

public:
	//CTOR
	MainCameraCharacterTargetState(Camera* camRef) : MainCameraState(CameraStateMode::PLAYABLE_CHARACTER, camRef) {};

	//called from the FSM when entering this camera state
	void OnCamStateEnter() override;

	//called from the FSM when exiting this camera state
	void OnCamStateExit() override;

	//called for camera updates from the FSM. 
	//it is called after all rendering is done to update camera properties for the next frame 
	void OnPostRenderUpdate(Math::Float32 time_delta) override;

	//sets the playable character reference of this state
	inline void SetPlayableCharacterReference(PlayableCharacter* pc) 
	{
		m_character = pc;
	}

private:

	//get the cam target position from the player velocity and according to the offsets specified on the current cam properties
	Math::Vector3D GetCamTargetPosFromPlayerVel(Math::Vector3D playerVel);

	//clamp a position to be at maximum to the offset (XY) specified on the character cam properties
	Math::Vector3D ClampPosToDefaultOffsetFromPlayer(Math::Vector3D pos);

	//sets the camera target position to (pos.X, pos.Y) but the depth is measured from the character and its cam settings
	void SetCamTargetPositionTo(Math::Vector3D pos);

};

#endif

