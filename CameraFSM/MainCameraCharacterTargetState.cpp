//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------


#include "MainCameraCharacterTargetState.h"

//called from the FSM when entering this camera state
void MainCameraCharacterTargetState::OnCamStateEnter() 
{
	//at start of this camera state, target the center of the character
	SetCamTargetPositionTo(m_character->GetWorldPosition());
}

//called from the FSM when exiting this camera state
void MainCameraCharacterTargetState::OnCamStateExit() 
{
}

//called for camera updates from the FSM. 
//it is called after all rendering is done to update camera properties for the next frame 
void MainCameraCharacterTargetState::OnPostRenderUpdate(Math::Float32 time_delta) 
{
	//get the player physics velocity
	Math::Vector3D playerVel = m_character->GetNormalizedCharaterVelocity();

	//get the current target position from the velocity of this character
	SetCamTargetPositionTo(ClampPosToDefaultOffsetFromPlayer(GetCamTargetPosFromPlayerVel(playerVel)));

	//get camera position with the character displacement that must be carried to compensate from the character displacement
	Math::Vector3D camWithDisplacement = m_CamRef->GetWorldPosition() + m_character->GetPlayerDisplacement();

	//lerp from the camera (after displacement due to character) to the current target position
	m_CamRef->SetWorldPosition(Math::Lerp(camWithDisplacement, m_camCurrentTargetPos, m_character->camSettings.motionSpeed, time_delta));
}

//get the cam target position from the player velocity and according to the offsets specified on the current cam properties
Math::Vector3D MainCameraCharacterTargetState::GetCamTargetPosFromPlayerVel(Math::Vector3D playerVel) 
{
	//the position in the X direction is the current pos plus the velocity of the character and the motion factor specified in the cam settings
	Math::Float32 posX = m_camCurrentTargetPos.x + playerVel.x * m_character->camSettings.camTargetMotionSpeedFactor;

	//the position in the Y direction is a vertical displacement from the character position. the displacement is specified in the cam settings
	Math::Float32 posY = m_character->GetWorldPosition().y + m_character->camSettings.offsetFromPlayer.y;

	//the position in the Z direction is left unchanged
	return Math::Vector3D(posX, posY, m_camCurrentTargetPos.z);
}

//clamp a position to be at maximum to the offset (XY) specified on the character cam properties
Math::Vector3D MainCameraCharacterTargetState::ClampPosToDefaultOffsetFromPlayer(Math::Vector3D pos) 
{
	//get the character world pos
	Math::Vector3D charPos = m_character->GetWorldPosition();

	//compute signed offsets from the char position to the input pos 
	Math::Vector3D currentOffsets(pos.x - m_character->GetWorldPosition().x, pos.y - m_character->GetWorldPosition().y, 0);

	//clamp positions
	pos.x = charPos.x + Math::Clamp(currentOffsets.x, -m_character->camSettings.offsetFromPlayer.x, m_character->camSettings.offsetFromPlayer.x);
	pos.y = charPos.y + Math::Clamp(currentOffsets.y, -m_character->camSettings.offsetFromPlayer.y, m_character->camSettings.offsetFromPlayer.y);

	//returns the modified input position
	return pos;
}

//sets the camera target position to (pos.X, pos.Y) but the depth is measured from the character and its cam settings
void MainCameraCharacterTargetState::SetCamTargetPositionTo(Math::Vector3D pos) 
{
	m_camCurrentTargetPos = { pos.x, pos.y, m_character->GetWorldPosition().z + m_character->camSettings.depth };
}
