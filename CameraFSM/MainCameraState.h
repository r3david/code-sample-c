//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MAIN_CAM_FSM_BASE_STATE_H
#define MAIN_CAM_FSM_BASE_STATE_H

#include "Camera.h"

//these are the different camera states we can have
enum class CameraStateMode : Math::Int32 
{
	NULL_TARGET = 0, //does nothing, used to implement the NULL OBJECT pattern on the FSM
	FIXED_TARGET = 1, //interpolate to a fixed target 
	PLAYABLE_CHARACTER = 2, //camera for playable characters (smooth follow the character)
};

//MainCameraState:
//this implements the base class interface that any state of the FSM of the main camera has to derive from
class MainCameraState 
{
protected:
	//weak reference to the camera this state is driving
	Camera* m_CamRef;

	//the camera mode of this state
	const CameraStateMode m_CamMode;

protected:
	//CTOR: sets the camera mode of this state and the camera reference
	MainCameraState(CameraStateMode mode, Camera* camRef) : m_CamMode(mode), m_CamRef(camRef) { }

public:
	//DTOR
	virtual ~MainCameraState() {};

	//called from the FSM when entering this camera state
	virtual void OnCamStateEnter() = 0;

	//called from the FSM when exiting this camera state
	virtual void OnCamStateExit() = 0;

	//called for camera updates from the FSM. 
	//it is called after all rendering is done to update camera properties for the next frame 
	virtual void OnPostRenderUpdate(Math::Float32 time_delta) = 0;

	//returns the camera mode of this state
	inline CameraStateMode GetCameraMode() const 
	{
		return m_CamMode;
	}

};

#endif
