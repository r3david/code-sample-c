//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------


#include "MainCameraStateMachine.h"

//performs allocation of the FSM. called from the entry point of the level/scene
void MainCameraStateMachine::Initialize(Camera* cameraRef) 
{
	//initialize array of states for the camera
	m_CameraStates[static_cast<Math::Int32>(CameraStateMode::NULL_TARGET)] = new MainCameraNullTargetState();
	m_CameraStates[static_cast<Math::Int32>(CameraStateMode::FIXED_TARGET)] = new MainCameraFixedTargetState(cameraRef);
	m_CameraStates[static_cast<Math::Int32>(CameraStateMode::PLAYABLE_CHARACTER)] = new MainCameraCharacterTargetState(cameraRef);

	//default on null motion
	m_CurrentCamStateIdx = static_cast<Math::Int32>(CameraStateMode::NULL_TARGET);
}

//deallocation of the FSM. called to shut down the camera
//each camera state pointer is deallocated. 
//the fixed size array itself is automatically deallocated
void MainCameraStateMachine::DeInitialize() 
{
	//delete manually all state pointers
	for (int i = 0; i<NUMBER_OF_STATES; ++i) 
	{
		delete m_CameraStates[i];
	}
}

//called by the engine after all rendering has been done
//updates the camera according to the current state of this machine
void MainCameraStateMachine::OnPostRender(Math::Float32 time_delta)
{
	m_CameraStates[m_CurrentCamStateIdx]->OnPostRenderUpdate(time_delta);
}

//transits to the state of Fixed target with the specified configuration
//if the camera is on that state already, then the fixed target state will 
//receive the new config, but the transition to new state is not actually done
void MainCameraStateMachine::SetCameraStateToFixedTarget(const CamFixedTargetModeConfig & config) 
{
	//set the new configuration of the fixed target
	dynamic_cast<MainCameraFixedTargetState*>(m_CameraStates[static_cast<Math::Int32>(CameraStateMode::FIXED_TARGET)])->SetFollowConfiguration(config);

	//transit to the fixed target state
	SetToState(CameraStateMode::FIXED_TARGET);
}

//transits to the state of the playable character specified
void MainCameraStateMachine::SetCameraStateToPlayableCharacter(PlayableCharacter* pc) 
{
	//set the character reference
	dynamic_cast<MainCameraCharacterTargetState*>(m_CameraStates[static_cast<Math::Int32>(CameraStateMode::PLAYABLE_CHARACTER)])->SetPlayableCharacterReference(pc);

	//transit to the playable character camera state
	SetToState(CameraStateMode::PLAYABLE_CHARACTER);
}

//method of transitions to a new states
void MainCameraStateMachine::SetToState(CameraStateMode newState) 
{
	//do nothing if we are already on that state
	if (m_CurrentCamStateIdx == static_cast<int>(newState)) 
		return;

	//otherwise exit current state
	m_CameraStates[m_CurrentCamStateIdx]->OnCamStateExit();

	//set the current state
	m_CurrentCamStateIdx = static_cast<int>(newState);

	//enter new camera state
	m_CameraStates[m_CurrentCamStateIdx]->OnCamStateEnter();
}
