//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com), 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MAIN_CAM_FSM
#define MAIN_CAM_FSM

#include "MainCameraState.h"
#include "MainCameraNullTargetState.h"
#include "MainCameraFixedTargetState.h"
#include "MainCameraCharacterTargetState.h"

//MainCameraStateMachine: 
//implement a finite state machine for the main camera, including methods to transit from one state to another
class MainCameraStateMachine 
{
private:
	//the number of states for this FSM
	static const int NUMBER_OF_STATES = 3;

	//Fixed size array with all possible camera states 
	MainCameraState* m_CameraStates[NUMBER_OF_STATES];

	//current camera state index
	Math::Int32 m_CurrentCamStateIdx;

public:
	//CTOR: the constructor will do nothing,
	//all initialization is deferred to the Initialize method.
	MainCameraStateMachine() {};

	//DTOR: does nothing. all deallocation is done on the Deinitialize method.
	~MainCameraStateMachine() {};

	//performs allocation of the FSM. For example can be called from the entry point of the level/scene
	void Initialize(Camera* cameraRef);

	//deallocation of the FSM. called to shut down the camera
	void DeInitialize();

	//called by the engine after all rendering has been done
	//updates the camera according to the current state of this machine
	void OnPostRender(Math::Float32 time_delta);

	//transits to the state of Fixed target with the specified configuration
	//if the camera is on that state already, then the fixed target state will receive the new config, 
	//but the transition to new state is not actually done
	void SetCameraStateToFixedTarget(const CamFixedTargetModeConfig & config);

	//transits to the state of the playable character specified
	//if the camera is on that state already, then the state will dereference to the new playable character
	void SetCameraStateToPlayableCharacter(PlayableCharacter* pc);

private:
	//transition to a new state
	void SetToState(CameraStateMode newState);
};

#endif

